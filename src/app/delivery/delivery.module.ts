import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DeliverydashComponent } from './deliverydash/deliverydash.component';
import { DeliveryRoutingModule } from './delivery-routing.module';
import { SharedModule } from '../shared';
import { OrderparketagesComponent } from './orderparketages/orderparketages.component';
import { OrderdeliveryComponent } from './orderdelivery/orderdelivery.component';
import { OrderdeliveredComponent } from './orderdelivered/orderdelivered.component';
import { ProfileComponent } from './profile/profile.component';



@NgModule({
  declarations: [
    DeliverydashComponent,
    OrderparketagesComponent,
    OrderdeliveryComponent,
    OrderdeliveredComponent,
    ProfileComponent
  ],
  imports: [
    SharedModule,
    DeliveryRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class DeliveryModule { }
