import { Component, OnInit,Input,OnChanges, SimpleChanges } from '@angular/core';
import { ApiService, EventparserService } from 'src/app/core';

@Component({
  selector: 'app-orderparketages',
  templateUrl: './orderparketages.component.html',
  styleUrls: ['./orderparketages.component.scss']
})
export class OrderparketagesComponent implements OnInit,OnChanges {
 @Input() prod_ended:any=[]
  ordre:any
  element:any=[]
  search:any
  constructor(private http:ApiService, private com:EventparserService) { }

  ngOnInit(): void {

    this.element=this.prod_ended
  }
  ngOnChanges(): void {
    this.element=this.prod_ended 
  }
UpdateStatus(event:any){
var pending:any=[]
var endpending:any=[]
let id=event.target.id
if(id!=undefined){
let data ={id:id, status:"pending delivery"}
this.http.setStatus(data.id, data.status).subscribe(res=>{
  console.log(res)
  this.http.getOrders().subscribe(res=>{
    console.log(res)
    var data:any=res.data

    for(let item of data){
      if(item.userorder_status.toLowerCase()==='pending at production'){
      pending.push(item)
      }
      if(item.userorder_status.toLowerCase()==='pending delivery'){
        endpending.push(item)
      }
    }
    this.element=pending
    
    if(endpending.length>0){
      this.com.update_pending_delivery.next({data:endpending, endpend:true})
    }
  })
},err=>{
  console.log(err)
})
}
}

inputSearch(){
  if(this.search){
    this.element= this.prod_ended.filter((product:any)=>{
      if(product.userorder_id.toString().includes(this.search.toString()) ||
     product.user_first_name.toLowerCase().includes(this.search.toLowerCase())){
        return product
      }else{
        this.element=this.prod_ended
      }
    })
  }
}
}
