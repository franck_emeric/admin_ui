import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderparketagesComponent } from './orderparketages.component';

describe('OrderparketagesComponent', () => {
  let component: OrderparketagesComponent;
  let fixture: ComponentFixture<OrderparketagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderparketagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderparketagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
