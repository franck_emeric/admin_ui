import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  photo=true
  viewimage:any
  newphoto=false
  
  constructor() { }

  ngOnInit(): void {
  }
 Onchange(event:any){
   let file= event.target.files[0]

   var reader= new FileReader();
   reader.onload=()=>{
    this.viewimage=reader.result;
    this.photo=false
    this.newphoto=true
   };
   reader.readAsDataURL(file)
 }


}
