import { Component, OnInit,Input } from '@angular/core';
import { ApiService, EventparserService } from 'src/app/core';

@Component({
  selector: 'app-orderdelivery',
  templateUrl: './orderdelivery.component.html',
  styleUrls: ['./orderdelivery.component.scss']
})
export class OrderdeliveryComponent implements OnInit {
@Input() delivery:any=[]
element:any=[]
search:any
  constructor(private http:ApiService, private com:EventparserService) { }

  ngOnInit(): void {

    this.element=this.delivery
  }
UpdateStatus(event:any){
  let delivred:any=[]
  let pendlivery:any=[]
  let id =event.target.id
  if(id!=undefined){
    let data:any={id:id, status:"Delivered"}
    this.http.setStatus(data.id, data.status).subscribe(res=>{
     
      this.http.getOrders().subscribe(res=>{
        var data:any =res.data
        for(let item of data){
          if(item.userorder_status.toLowerCase()==='pending delivery'){
            pendlivery.push(item)
          }
          if(item.userorder_status.toLowerCase()==='Delivered'){
            delivred.push(item)
          }
        }
        this.element=pendlivery;

        if(delivred.length>0){
          this.com.uddate_delivered_orders.next({data:delivred, delivered:true})
        }
      })

    },err=>{
      console.log(err)
    })
  }
}
inputSearch(){
  if(this.search){
    this.element=this.delivery.filter((product:any)=>{
      if(
        product.userorder_id.toString().includes(this.search.toString()) ||
        product.user_last_name.toLowerCase().includes(this.search.toLowerCase())
      ){
        return product
      }else{
        this.element=this.delivery
      }
    })
  }
}

}
