import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderdeliveryComponent } from './orderdelivery.component';

describe('OrderdeliveryComponent', () => {
  let component: OrderdeliveryComponent;
  let fixture: ComponentFixture<OrderdeliveryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderdeliveryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderdeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
