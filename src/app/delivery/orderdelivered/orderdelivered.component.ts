import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/core';
@Component({
  selector: 'app-orderdelivered',
  templateUrl: './orderdelivered.component.html',
  styleUrls: ['./orderdelivered.component.scss']
})
export class OrderdeliveredComponent implements OnInit {
@Input() delivered:any=[]
search:any
element:any=[]
  constructor(private http:ApiService) { }

  ngOnInit(): void {
   this.element=this.delivered
  }
inputSearch(){
if(this.search){
this.element=this.delivered.filter((product:any)=>{
 if(
   product.userorder_id.toString().includes(this.search.toString()) ||
   product.user_first_name.toLowerCase().includes(this.search.toLowerCase())
   ){
  return product
 }

});
}else{
this.element=this.delivered
}

}
}
