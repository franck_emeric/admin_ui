import { Component, OnInit } from '@angular/core';
import { ApiService, EventparserService } from 'src/app/core';

@Component({
  selector: 'app-deliverydash',
  templateUrl: './deliverydash.component.html',
  styleUrls: ['./deliverydash.component.scss']
})
export class DeliverydashComponent implements OnInit {
  packatage=true
  delivery=false
  delivered=false
  profile=false
  prodended:any=[]
  pendingdelivry:any=[]
  Delivered:any=[]

 menu={
  name:"packservice",
  user:"Emeric",
  role:"Packages_service"
 }
  constructor(private Event:EventparserService, private http:ApiService) { }

  ngOnInit(): void {
    this.http.getOrdersByStatus("production ended").subscribe(res=>{
      console.log(res);
      var prod_ended:any=res
      if(prod_ended.status){
        this.prodended=prod_ended.data
      }
      
    },err=>{
      console.log(err)
    })
    this.http.getOrdersByStatus("pending delivery").subscribe(res=>{
      console.log(res);
      var pend_delivery:any=res
      if( pend_delivery.status){
        this.pendingdelivry= pend_delivery.data
      }
      
    },err=>{
      console.log(err)
    })
    this.http.getOrdersByStatus("Delivered").subscribe(res=>{
      console.log(res);
      var delivered:any=res
      if(delivered.status){
        this.Delivered= delivered.data
      }
      
    },err=>{
      console.log(err)
    })
    this.Event.update_pending_delivery.subscribe(res=>{
      console.log(res)
     var data:any= res
     if(data.endpend){
       this.pendingdelivry=data.data
     }
    },err=>{
      console.log(err)
    })
    this.Event.uddate_delivered_orders.subscribe(res=>{
      console.log(res)
      var data:any=res
      if(data.delivered){
        this.Delivered=data.data
      }
    },err=>{
      console.log(err)
    })
    this.Event.profiles.subscribe(menu=>{
      this.profile=menu
      this.packatage=false
      this.delivery=false
      this.delivered=false
  
    })
    this.Event.delevered.subscribe(menu=>{
      this.profile=false
      this.packatage=false
      this.delivery=false
      this.delivered=menu
  
    })
    this.Event.end_pack_order.subscribe(menu=>{
      this.profile=false
      this.packatage=false
      this.delivery=menu
      this.delivered=false
  
    })
    this.Event.pack_order.subscribe(menu=>{
      this.profile=false
      this.packatage=menu
      this.delivery=false
      this.delivered=false
  
    })

  }
  


}
