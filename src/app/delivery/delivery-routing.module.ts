import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeliverydashComponent } from './deliverydash/deliverydash.component';

const routes: Routes = [
  {path:"",component:DeliverydashComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DeliveryRoutingModule { }
