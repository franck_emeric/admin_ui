import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductedPendingComponent } from './producted-pending.component';

describe('ProductedPendingComponent', () => {
  let component: ProductedPendingComponent;
  let fixture: ComponentFixture<ProductedPendingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductedPendingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductedPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
