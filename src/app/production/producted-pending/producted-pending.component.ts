import { Component, Input, OnInit } from '@angular/core';
import { ApiService, EventparserService } from 'src/app/core';

@Component({
  selector: 'app-producted-pending',
  templateUrl: './producted-pending.component.html',
  styleUrls: ['./producted-pending.component.scss']
})
export class ProductedPendingComponent implements OnInit {
@Input() pendingOrders:any=[]
Pending:any=[]
orderpending:any
  constructor(private http:ApiService, private com:EventparserService) { }

  ngOnInit(): void {
   
  }
UpdateStatus(event:any){
  let endedprod:any=[]
  let pendingorder:any=[]
  let id=event.target.id
  if(id!=undefined){
    let data:any={id:id,status:"production ended"}
    this.http.setStatus(data.id ,data.status).subscribe(res=>{
      console.log(res);
      this.http.getOrders().subscribe(res=>{
        console.log(res)
        var data:any=res.data

        for(let item of data){
          if(item.userorder_status.toLowerCase()==='pending at production'){
          pendingorder.push(item)
          }
          if(item.userorder_status.toLowerCase()==='production ended'){
            endedprod.push(item)
          }
        }
        this.pendingOrders=pendingorder
        
        if(endedprod.length>0){
          this.com.update_ended_prod.next({data:endedprod, end:true})
        }
      })
    },err=>{
      console.log(err)
    })
  }
}
}
