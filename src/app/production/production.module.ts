import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../shared';
import { OrdersComponent } from './neworders/neworders.component';
import { ProductionRoutingModule } from './production-routing.module';
import { ProductiondashboardComponent } from './productiondashboard/productiondashboard.component';
import { ProductedItemsComponent } from './producted-items/producted-items.component';
import { ProductorComponent } from './productor/productor.component';
import { ProductedPendingComponent } from './producted-pending/producted-pending.component';

@NgModule({
  declarations: [
    OrdersComponent,
    ProductiondashboardComponent,
    ProductedItemsComponent,
    ProductorComponent,
    ProductedPendingComponent
  ],
  imports: [
    SharedModule,
    ProductionRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]

})
export class ProductionModule { }
