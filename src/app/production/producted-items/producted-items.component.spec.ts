import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductedItemsComponent } from './producted-items.component';

describe('ProductedItemsComponent', () => {
  let component: ProductedItemsComponent;
  let fixture: ComponentFixture<ProductedItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductedItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductedItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
