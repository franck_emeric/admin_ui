import { Component, OnInit } from '@angular/core';
import { ApiService, EventparserService } from 'src/app/core';
import { TokenStorageServiceService } from 'src/app/core/auths';
@Component({
  selector: 'app-productiondashboard',
  templateUrl: './productiondashboard.component.html',
  styleUrls: ['./productiondashboard.component.scss']
})
export class ProductiondashboardComponent implements OnInit {
  menu =
  {
   name:"production", 
   user:"dave",
   role:"prod manager"
 }
 default=true
 ended=false
 pend=false
 user=false
 newOrders:any=[];
 pendingOrders:any=[];
 prodOrders:any=[];


  constructor(private Myevent:EventparserService,private tokenService:TokenStorageServiceService,private api:ApiService) { }

  ngOnInit(): void {

    this.Myevent.new_orders.subscribe(
      menu=>{
        this.default=menu;
        this.user=false;
        this.ended=false
        this.pend=false
      }
    )
    this.Myevent.logout.subscribe(
      res=>{
        if(res){
          this.tokenService.signOut();
          setTimeout(()=>{
            location.href="/login"
          },1000)
         
        }
      }
    )

    this.Myevent.producted_item.subscribe(
      menu=>{
        this.default=false;
        this.user=false;
        this.ended=menu;
        this.pend=false
      }
    )


    this.Myevent.profile.subscribe(
      menu=>{
        this.default=false;
        this.user=menu;
        this.ended=false;
        this.pend=false
      }
    )

    this.Myevent.pending.subscribe(menu=>{
      this.default=false;
      this.user=false;
      this.ended=false;
      this.pend=true
    })

    this.tokenService.getUser().then(
      (user)=>{
        console.log(user)
       if(user)
       this.menu.user=user.name;
       this.menu.role=user.role.role_name
      }
    ).catch((err)=>{
      console.log(err)
    })


    this.api.getOrdersByStatus("new").subscribe(
      res=>{
        console.log(res)
        var resp:any=res
        if(resp.status){
          this.newOrders=resp.data;

        }
      },
      err=>{
        console.log(err)
      }
    )

    this.api.getOrdersByStatus("pending at production").subscribe(
      res=>{
        console.log(res)
        var resp:any=res
        if(resp.status){
          this.pendingOrders=resp.data
        }
      },
      err=>{
        console.log(err)
      }
    )

    this.api.getOrdersByStatus("production ended").subscribe(
      res=>{
        console.log(res)
        var resp:any=res
        if(resp.status){
          this.prodOrders=resp.data
        }
      },
      err=>{
        console.log(err)
      }
    )


    this.Myevent.update_pending_order.subscribe(res=>{
      console.log(res)
     var data:any= res
     if(data.pending){
       this.pendingOrders=data.data
     }
    },err=>{
      console.log(err)
    })
    this.Myevent.update_ended_prod.subscribe(res=>{
      console.log(res)
     var data:any= res
     if(data.end){
       this.prodOrders=data.data
     }
    },err=>{
      console.log(err)
    })
  }

}
