import { Component, Input, OnInit } from '@angular/core';
import { ApiService, EventparserService } from 'src/app/core';

@Component({
  selector: 'app-neworders',
  templateUrl: './neworders.component.html',
  styleUrls: ['./neworders.component.scss']
})
export class OrdersComponent implements OnInit {
@Input() newOrders:any=[]
orders:any=[]
ordre:any
  constructor(private http:ApiService, private com:EventparserService) { }

  ngOnInit(): void {
  
  }
updateStatus(event:any){
  let neworder:any=[]
  let pendingorder:any=[]
  let id =event.target.id
  console.log(id)
  if(id!=undefined){
    let data={id:id, status:"pending at production"}
    this.http.setStatus(data.id, data.status).subscribe(res=>{
      
      this.http.getOrders().subscribe(res=>{
        console.log(res)
        var data:any=res.data

        for(let item of data){
          if(item.userorder_status.toLowerCase()==='pending at production'){
          pendingorder.push(item)
          }
          if(item.userorder_status.toLowerCase()==='new'){
            neworder.push(item)
          }
        }
        this.newOrders=neworder
        
        if(pendingorder.length>0){
          this.com.update_pending_order.next({data:pendingorder, pending:true})
        }
      })
     
    },err=>{
      console.log(err)
    })
  }
}
}
