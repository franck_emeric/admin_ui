import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AthAdminGuard } from '../core/auths/guards/auth-admin';
import { ProductionGuard } from '../core/auths/guards/production/production.guard';
import { ProductiondashboardComponent } from './productiondashboard/productiondashboard.component';
const routes: Routes = [
    {
    path:'',component:ProductiondashboardComponent,//canActivate:[ProductionGuard]
    }
    ,
    {
      path:'me',component:ProductiondashboardComponent,canLoad:[AthAdminGuard]
      }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionRoutingModule { }
