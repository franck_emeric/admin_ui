import { Component, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core/communications';
import { ApiService, } from 'src/app/core/httpservices/api.service';
import { LoginService } from 'src/app/core/auths';
import { LZString } from 'lzstring.ts/build/LZString';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
 redirectpaths={
   production:"production/manager",
   customerservce:"customer/services",
   designer:"designer",
   partner:"partner",
   admin:"me",
   stock:"stock/manager",
   marketing:"marketing/manager",
   packs_services:"delivery"
 }

 userapp=false
 data:any={
   name:"dave"
 }
user={
 email:"" ,
 password:"",
}
loading=false
  constructor(private myevent:EventparserService,private http:ApiService,private auth:LoginService) { }

  ngOnInit(): void {
  
   this.myevent.loaded.subscribe(
    res=>{

      this.loading=res
    
    } 
   ) 

   this.myevent.isloggedInAsSuperAdmin.subscribe(mess=>{
    this.userapp=mess;

   })

   this.myevent.userData.subscribe(data=>{
    this.data=data
    console.log(data)
   })
  }


  login(){
  this.auth.login(this.user)
  }


  
}
