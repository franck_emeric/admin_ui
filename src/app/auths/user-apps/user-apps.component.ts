import { Component, Input, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core';
@Component({
  selector: 'app-user-apps',
  templateUrl: './user-apps.component.html',
  styleUrls: ['./user-apps.component.scss']
})
export class UserAppsComponent implements OnInit {
@Input() user:any
  constructor(private ep:EventparserService) { }

  ngOnInit(): void {
    window.location.href="/me"
    this.menu()
  }
  designerapp(){
  this.ep.userData.next(
    {
      name:this.user.info.name,
      role:this.user.role
    }
  )
  }
productionapp(){
  this.ep.userData.next(
    {
      name:this.user.info.name,
      role:this.user.role
    }
  )
}




  menu=()=>{
    var items = document.querySelectorAll('.circle-menu-box a.menu-item');

    for(var i = 0, l = items.length; i < l; i++) {
      items[i].setAttribute("left",(40 - 35*Math.cos(-0.5 * Math.PI - 2*(1/l)*i*Math.PI)).toFixed(4) + "%");
      
      items[i].setAttribute("top", (40 + 35*Math.sin(-0.5 * Math.PI - 2*(1/l)*i*Math.PI)).toFixed(4) + "%");
    }
  }

}
