import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService, EventparserService } from 'src/app/core';
import { LoginService, RegisterService } from 'src/app/core/auths';
declare var require:any
var $ = require("jquery")
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  role=["Designer","CEO","COO","DM","CC"];
  roles:any
  nom:any
  prenom:any
  telephone:any
  email:any
  user_role:any;
  sex:any
  password:any;
  confpwd:any
  message:any
error=false
loading=false
  constructor(private route: ActivatedRoute,private api:ApiService,private Auth:RegisterService,private evnt:EventparserService) { }

  ngOnInit(): void {
    var token=this.route.snapshot.params.token
    this.user_role=this.parseJwt(token)
    console.log(this.user_role)
    this.evnt.loaded.subscribe(rs=>{
     this.loading=!rs
    })
  }


  parseJwt (token:any) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};


onchange(event:any)
{
if(event.target.value=="femme")
{
  this.sex=2
}
if(event.target.value=="homme"){
  this.sex=1

}

}
    register(){
      var data={
        last_name:this.prenom,
        first_name:this.nom,
        phone:this.telephone,
        email:this.email,
        sex:this.sex,
        password:this.password,
        perm:this.user_role.perm,
        role:this.user_role.role
      }
      if(this.password==this.confpwd)
      {
        this.toggle()
        this.api.saveUser(data).subscribe(res=>{
          var resp=res
          console.log(res)
          if(resp.status)
          {
           this.Auth.auth({password:this.password,email:this.email})
          }
        },err=>{
          this.toggle()
          this.message="une erreur s'est produite"
          this.error=true

          console.log(err)
        })
      }else
      {
        this.error=true
        this.message="les  mots de passe sont pas conformes"
      }
      
     } 

    Cancel(eve:boolean) {
      this.error=eve
    }

    toggle(){
      this.loading=!this.loading
    }

}
