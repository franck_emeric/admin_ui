import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenIsExpiredComponent } from './token-is-expired.component';

describe('TokenIsExpiredComponent', () => {
  let component: TokenIsExpiredComponent;
  let fixture: ComponentFixture<TokenIsExpiredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TokenIsExpiredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenIsExpiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
