import {  NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanRegisterGuard } from '../core/auths/guards/register';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TokenIsExpiredComponent } from './token-is-expired/token-is-expired.component';
const routes: Routes = [
  {path:'login', component:LoginComponent},
  {path:'register/:token', component:RegisterComponent,canActivate:[CanRegisterGuard]},
  {path:"link-expired", component:TokenIsExpiredComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthsRoutingModule { }
