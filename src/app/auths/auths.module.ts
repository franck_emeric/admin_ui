import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RecoverypwdComponent } from './recoverypwd/recoverypwd.component';
import { AuthsRoutingModule } from './auths-routing.module';
import { SharedModule } from '../shared';
import { LoginHeaderComponent } from './login-header/login-header.component';
import { AuthInterceptorService, LoginService } from '../core/auths';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserAppsComponent } from './user-apps/user-apps.component';
import { TokenIsExpiredComponent } from './token-is-expired/token-is-expired.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    RecoverypwdComponent,
    LoginHeaderComponent,
    UserAppsComponent,
    TokenIsExpiredComponent,
    AlertComponent
  ],

  imports: [
    AuthsRoutingModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthsModule { }
