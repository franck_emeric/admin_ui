import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockmdashComponent } from './stockmdash.component';

describe('StockmdashComponent', () => {
  let component: StockmdashComponent;
  let fixture: ComponentFixture<StockmdashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockmdashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockmdashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
