import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockmdashComponent } from './stockmdash/stockmdash.component';

const routes: Routes = [
  {path:"",component:StockmdashComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class StockManagementRoutingModule { }
