import { NgModule } from '@angular/core';
import { StockmdashComponent } from './stockmdash/stockmdash.component';
import { SharedModule } from '../shared';
import { StockManagementRoutingModule } from './stockmanagement-routing.module';



@NgModule({
  declarations: [
    StockmdashComponent
  ],
  imports: [
    SharedModule,
    StockManagementRoutingModule
  ]
})
export class StockmanagementModule { }
