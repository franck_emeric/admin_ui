import { Component, OnInit,Input,ViewChild,OnChanges,SimpleChanges } from '@angular/core';
import { fabric } from 'fabric';
import { AladinService, ApiService } from 'src/app/core';
import Swal from 'sweetalert2';
import { ColorEvent } from 'ngx-color';
import { compress} from 'compress-json'
import { ContextMenuComponent } from 'ngx-contextmenu';
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit,OnChanges {
@Input() data:any;
@Input() Items:any=[];
 default=true;
  canvas:any
  pallette=false;
  produite=true;
  modeles=false
  formes=false
  design=false
  width=300
  height=300
  vc_height=300;
  vc_width=500
  k_width=300;
  k_height=500
  v_height=250
  v_width=250
  b_m_width=600;
  b_m_height=400
  fl_width=500;
  fl_height=600;
  g_height=400
  g_width=300;
  p_width=200;
  p_height=300

  passive=true
  divider=true
  cpt=0;
  @Input() usr:any;
  cptr=0;
  isRedoing=false;
  state:any=[]
 @Input() default_data:any=[]
  text_width=30
  @Input() category:any
  @Input() type:any
  textalign=["left","center","justify","right"]
  url1="/assets/images/indx.png";
  text="aladin"
  title="titre de mon design"
  public items = { name: 'John', otherProperty: 'Foo' }
  canvaspages:any=[]
   currentItem:number|any;
   backUrl=[{url:"/assets/images/kakemono.jpg"},{url:"/assets/images/kake.jpeg"},{url:"/assets/images/6.png"},{url:"/assets/images/7.png"},{url:"/assets/images/index.jpeg"},{url:"/assets/images/bgd.jpeg"},{url:"/assets/images/gd.jpg"},{url:"/assets/images/about.jpeg"},{url:"/assets/images/abt.jpeg"},{url:"/assets/images/abtt.jpeg"},{url:"/assets/images/gbd.jpeg"},{url:"/assets/images/aff.jpg"},{url:"/assets/images/card.jpg"},{url:"/assets/images/acrdv.jpeg"},{url:"/assets/images/c.jpeg"},{url:"/assets/images/flr.jpeg"},{url:"/assets/images/1.png"},{url:"/assets/images/2.png"},{url:"/assets/images/3.png"},{url:"/assets/images/4.png"}]
   colors=["blue","white","black","red","orange","yellow","green","#999999","#454545","#800080","#000080","#00FF00","#800000","#008080","#324161","#fab91a","brown"]
   shape=[`https://img.icons8.com/color/96/000000/address--v1.png`,`https://img.icons8.com/office/80/000000/facebook-new.png`,`https://img.icons8.com/color/96/000000/fragile.png`,`https://img.icons8.com/office/80/000000/linkedin-circled--v2.png`,`https://img.icons8.com/color/96/000000/delivery--v2.png`,`https://img.icons8.com/color/48/000000/shipped.png`,`https://img.icons8.com/color/96/000000/manual-handling.png`,`https://img.icons8.com/color/48/000000/warranty.png`,`https://img.icons8.com/flat-round/96/000000/warranty.png`,`https://img.icons8.com/color/96/000000/sale--v1.png`,`https://img.icons8.com/color/48/000000/shopaholic.png`,`https://icons8.com/illustrations`] 
   fonts = [
   {name:"Flowerheart",url:"./assets/fonts/FlowerheartpersonaluseRegular-AL9e2.otf"},
   {name:"HussarBold",url:"./assets/fonts/HussarBoldWebEdition-xq5O.otf"},
   {name:'Branda',url:"./assets/fonts/Branda-yolq.ttf"},
   {name:"MarginDemo",url:"./assets/fonts/MarginDemo-7B6ZE.otf"},
   {name:"Kahlil",url:"./assets/fonts/Kahlil-YzP9L.ttf"},
   {name:"FastHand",url:"./assets/fonts/FastHand-lgBMV.ttf"},
   {name:"BigfatScript",url:"./assets/fonts/BigfatScript-jE96G.ttf"},
   {name:"Atlane",url:"./assets/fonts/Atlane-PK3r7.otf"},
   {name:"HidayatullahDemo",url:"./assets/fonts/Bismillahscript-4ByyY.ttf"},
   {name:"Robus",url:"./assets/fonts/HidayatullahDemo-mLp39.ttf"},
   {name:"Backslash",url:"./assets/fonts/Robus-BWqOd.otf"},
   {name:"ChristmasStory",url:"./assets/fonts/ChristmasStory-3zXXy.ttf"},
  ];
 currentPage=""

conf={format:"jpeg",quality:0.8,multiplier:2} 
@Input() list_of_models:any=[]

 @ViewChild(ContextMenuComponent) public basicMenu:ContextMenuComponent | any
 @ViewChild(ContextMenuComponent) public PageMenu:ContextMenuComponent | any
  
 constructor(private aladin:AladinService,private api:ApiService) { 

  }

  ngOnInit(): void {
    this.canvas= new fabric.Canvas('aladin',{
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      stopContextMenu:false,
    });
   this.canvas.filterBackend = new fabric.WebglFilterBackend() 

   this.canvas.setWidth(this.width);
   this.canvas.setHeight(this.height);

   this.mouseActions()
   this.OnsetDrawing()
   setTimeout(
    ()=>{
    let back:any=[]
    let shp:any=[]
  if(this.Items.length>0){
    for(let i=0;i<this.Items.length;i++){

     if(this.Items[i].design_item_type.toLowerCase()==="background"){
      for (let el of JSON.parse(this.Items[i].design_item_urls)){
        back.push({url:el})

      }

     }

     if(this.Items[i].design_item_type.toLowerCase()==="shapes or cliparts"){
      for (let el of JSON.parse(this.Items[i].design_item_urls)){
        shp.push(el)

      }
     }
     

    }

     this.backUrl= back.concat(this.backUrl)
      this.shape=shp.concat(this.shape)
  
   
  }
    },2000
  )
 
  }

  OnsetDrawing(){
    if(this.data !=undefined){

      for(let item of this.category){
       if(this.data.category_label.toLowerCase()===item.category_label.toLowerCase() && item.category_label.toLowerCase()!="affichages" && item.category_label.toLowerCase()!="imprimes" && item.category_label.toLowerCase()!="gadgets" && item.category_label.toLowerCase()!="emballages" && item.category_label.toLowerCase()!="vetements"){
         this.load(this.data)  
          break ;
         }
         if(this.data.category_label.toLowerCase()=== item.category_label.toLowerCase() && item.category_label.toLowerCase()==="affichages" && item.category_label.toLowerCase()!="imprimes" &&item.category_label.toLowerCase()!="gadgets" && item.category_label.toLowerCase()!="emballages"  && item.category_label.toLowerCase()!="vetements"){ 
           if(this.data.productype_label.toLowerCase()==="kakemono"){
             this.canvas.setWidth(this.k_width)
             this.canvas.setHeight(this.k_height)
             this.load(this.data)   
             break
   
           }
           if(this.data.productype_label.toLowerCase()==="vinyle"){
             this.canvas.setWidth(this.v_width)
             this.canvas.setHeight(this.v_height)
             this.load(this.data)   
             break
   
           }
           if(this.data.productype_label.toLowerCase()==="bache"){
             this.canvas.setWidth(this.b_m_width)
             this.canvas.setHeight(this.b_m_height)
             this.load(this.data) 
             break  
   
           }
           if(this.data.productype_label.toLowerCase()==="micro perfore"){
             this.canvas.setWidth(this.b_m_width)
             this.canvas.setHeight(this.b_m_height)
             this.load(this.data)  
             break 
   
           }
   
         }
 
         if(this.data.category_label.toLowerCase()===item.category_label.toLowerCase() && item.category_label.toLowerCase()!="gadgets" && item.category_label.toLowerCase()!="emballages" && item.category_label.toLowerCase()!="vetements" && item.category_label.toLowerCase()!="affichages"){
           if(this.data.productype_label.toLowerCase()==="carte de visite"){
             this.canvas.setWidth(this.vc_width)
             this.canvas.setHeight(this.vc_height)
              this.load(this.data)  
              break 
   
           }
           if(this.data.productype_label.toLowerCase()==="flyer"){
            this.canvas.setWidth(this.fl_width)
            this.canvas.setHeight(this.fl_height)
             this.load(this.data)  
             break 
  
          }
         }
 
         if(this.data.category_label.toLowerCase()===item.category_label.toLowerCase() && item.category_label.toLowerCase()=="gadgets" && item.category_label.toLowerCase()!="emballages" && item.category_label.toLowerCase()!="vetements" && item.category_label.toLowerCase()!="affichages"){
             this.canvas.setWidth(this.g_width)
             this.canvas.setHeight(this.g_height)
              this.load(this.data)  
              break 
   
 
         }
 
         if(this.data.category_label.toLowerCase()===item.category_label.toLowerCase()){
           this.canvas.setWidth(this.p_width)
           this.canvas.setHeight(this.p_height)
            this.load(this.data)  
            break 
 
         
       }
 

      }
    }
  }
  showpallette(){
    this.design=false
    this.pallette=true;
     this.produite=false;
     this.modeles=false
     this.formes=false
  }

  mouseActions=()=>{
    this.canvas.on("object:added",()=>{
 
      if(!this.isRedoing){
        this.state = [];
      }
      this.isRedoing = false;
    
      
    })
  
    this.canvas.on("object:created",(e:any)=>{

     this.state.push(this.canvas.getActiveObject())
  
 
    })

    this.canvas.on("mouse:down",(e:any)=>{
      
    })



   this.canvas.on("mouse:move",(e:any)=>{
    if(!this.isRedoing){
      this.state = [];
    }
    this.isRedoing = false;

    // this.contextmenu(e)
     if(this.currentItem){
      this.currentPage=this.canvas.toDataURL(this.conf)
      this.canvaspages[this.currentItem-1].url= this.currentPage 
       this.canvaspages[this.currentItem-1].obj= this.canvas.toJSON()
      this.canvas.renderAll()
      this.canvas.requestRenderAll()

    }


   })
  
 
   this.canvas.on("after:render",(e:any)=>{
    if(this.canvas.getActiveObject()){
      if(this.canvas.getActiveObject().type==="textbox"){
        this.showpallette()

      }
       if(this.canvas.getActiveObject().type==="image") {
        this.showforme()
      }

      
    }

   })
  }
  
duplicatepage(){
  this.canvaspages.push(
    {
      url:this.canvas.toDataURL(this.conf),
      id:this.canvaspages.length+1,
      obj:this.canvas.toJSON()
    }
  )
  this.currentItem=this.canvaspages[this.canvaspages.length-1].id
}

removePage(id:any){
  this.canvaspages.splice(id,1)
  if(this.canvaspages.length>0){

  }
}

async load(item:any){
  this.canvaspages=[];
  this.currentPage=""
  if(!this.data){
    this.data=item
  }
  if(item.product_urls.length>1){
    for(let i=0;i<item.product_urls.length;i++){
      if(i>0){
          this.canvaspages.push({url:item.product_urls[i],id:i+1,obj:null})
      }else{
        await   this.aladin.loadpages(this.canvas,item.product_urls[i]).then(
          (data)=>{
            this.canvaspages.push({url:item.product_urls[i],id:i+1,obj:data})
            this.currentItem=1;
          }
        )
      
      }

    }
  }else{
    this.aladin.makeBackImage(item.product_urls[0],this.canvas).then((res)=>{
      this.canvaspages.push({url:item.product_urls[0],id:1,obj:res})
      this.currentItem=1;
    })
  }
}

  showforme(){
    this.design=false
    this.pallette=false;
     this.produite=false;
     this.modeles=false
     this.formes=true
  }
  showmodeles(){
    this.design=false
    this.pallette=false;
     this.produite=false;
     this.modeles=true
     this.formes=false
  }
  showproduite(){
    this.design=false
    this.pallette=false;
     this.produite=true;
     this.modeles=false
     this.formes=false
  }
  showdesign(){
    this.design=true
    this.pallette=false;
     this.produite=false;
     this.modeles=false
     this.formes=false
  }

 menupro=()=>{
   setTimeout(() => {
    let items = document.querySelectorAll('li');
    items.forEach(item => {
      item.addEventListener('click', (e) => {
      e.preventDefault()
         items.forEach(item => item.classList.remove('active'))
          item.classList.add('active')
        
      })
    })
   }, 100);
 }

 makeNewPage(){
 if(this.data.product_urls.length==1){
  if(this.cptr==0){
    this.canvaspages.push({url:this.canvas.toDataURL(this.conf),id:this.canvaspages.length+1,obj:this.canvas.toJSON()})
    this.cptr=this.cptr+1
    this.currentItem= this.canvaspages[this.canvaspages.length-1].id
  }
  else{
    this.canvas.clear();    
    this.canvaspages.push({url:this.canvas.toDataURL(this.conf),id:this.canvaspages.length+1,obj:this.canvas.toJSON()})
    this.currentItem= this.canvaspages[this.canvaspages.length-1].id

  }
 }else{
    this.canvas.clear();    
    this.canvaspages.push({url:this.canvas.toDataURL(this.conf),id:this.canvaspages.length+1,obj:this.canvas.toJSON()})
    this.currentItem= this.canvaspages[this.canvaspages.length-1].id
 }

 }

 newBackground(item:any){
  // console.log()
  this.data=item
  if(this.canvas.backgroundImage){
    Swal.fire({
      title: 'vous etes sur le point de changer l\'arrière plan?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, je veux le remplacer !',
      cancelButtonText:"Non"
    }).then((result) => {
      if (result.isConfirmed) {
        this.OnsetDrawing()
      }
    })
   }else{
    this.OnsetDrawing()

   }
 }



 changeBackground(url:any){
   if(this.canvas.backgroundImage){
    Swal.fire({
      title: 'vous etes sur le point de changer l\'arrière plan?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, je veux le remplacer !',
      cancelButtonText:"Non"
    }).then((result) => {
      if (result.isConfirmed) {
        this.aladin.makeBackImage(url,this.canvas)
  
      }
    })
   }else{
    this.aladin.makeBackImage(url,this.canvas)
   }
 
 }

 reactivatePage(item:any){
   if(item.obj===null){
    this.aladin.loadpages(this.canvas,item.url).then(
      (res)=>{
        this.currentItem=item.id
      }
    );
   
   }else{
    this.canvas.loadFromJSON(item.obj,()=>
    {
      this.currentItem=item.id;
      this.canvas.renderAll.bind(this.canvas);
    });
   }
 

    
 }


 makeBackground(url:any){
   this.changeBackground(url)
 }

 showMessage(param:any){
   console.log(param)
 }
 
 duplicate(){
  this.copy();
  this.paste()
}

copy(){
  this.aladin.copy(this.canvas)
}

paste(){
  this.aladin.paste(this.canvas)
}

setItem(url:any){
  this.aladin.makeItem(url,this.canvas)
}

textfont(item:any){
  let data= item.target.value
  this.aladin.textfont(Object.assign({},{name:data.substr(0,data.indexOf("*")),url:data.substr(data.indexOf("*")+1,data.length-1)}),this.canvas)
}


InputChange(){
  if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().text){
    if(this.cpt==0){
      this.text=this.canvas.getActiveObject().text+" "+ this.text;
      this.cpt=this.cpt+1
      this.canvas.getActiveObject().text= this.text;
      this.canvas.requestRenderAll();
    }else{
      if(this.text!=this.canvas.getActiveObject().text){
        this.canvas.getActiveObject().text= this.text;
        this.canvas.requestRenderAll();
      }
    
    }

  }else{  
    let text= new fabric.Textbox(this.text,{
      top:200,
      left:200,
      fill:"blue",
      fontSize:30,
      fontStyle:'normal',
      cornerStyle:'circle',
      selectable:true,
      borderScaleFactor:1,
      overline:false,
      lineHeight:1.5
    });
    this.canvas.add(text).setActiveObject(text);
    this.canvas.renderAll(text);
    this.canvas.requestRenderAll();
    this.canvas.centerObject(text);
  }
}


texteclor($event:ColorEvent){
  this.aladin.textcolor($event.color.hex,this.canvas);
  }


textwidth(){
  if(this.canvas.getActiveObject().text){
    console.log(this.canvas.getActiveObject())
    this.canvas.getActiveObject().set({height:this.canvas.getActiveObject().height+1})
    this.canvas.getActiveObject().set({width:this.canvas.getActiveObject().width+1})
    this.canvas.getActiveObject().set({fontSize:this.canvas.getActiveObject().fontSize+1})
    this.text_width= this.canvas.getActiveObject().fontSize
    this.canvas.renderAll()
  }
  
  }
  
  minus(){
    if(this.canvas.getActiveObject().text){
      console.log(this.canvas.getActiveObject())
      this.canvas.getActiveObject().set({height:this.canvas.getActiveObject().height-1})
      this.canvas.getActiveObject().set({width:this.canvas.getActiveObject().width-1})
      this.canvas.getActiveObject().set({fontSize:this.canvas.getActiveObject().fontSize-1})
      this.text_width= this.canvas.getActiveObject().fontSize
      this.canvas.renderAll()
    }
    
  }

  Redo(){
    if(this.state.length>0){
      this.isRedoing = true;
     this.canvas.add(this.state.pop());
    }
    
    }
    
    Undo(){
    if(this.canvas._objects.length>0){
      this.state.push(this.canvas._objects.pop());
      this.canvas.renderAll();
     }
}
  sendBack(){
    this.aladin.sendBack(this.canvas)
  }
  sendForward(){
    this.aladin.sendForward(this.canvas)
  }
  
  makeBold(){
    this.aladin.bold(this.canvas)
  }
  makeItalic(){
    this.aladin.italic(this.canvas)
  }

  textAlign(val:any){
   this.aladin.textAlign(this.canvas,val)
  }

  save(){
    var design_object:any=[]
    var urls :any=[]
    for(let item of this.canvaspages){
       design_object.push(
         item.obj
       )
       urls.push(
        item.url
      )
    } 
let compressed_data=compress(design_object);
let compressed_urls=compress(urls) 
if(this.canvaspages.length>0){
  var data={
    product:this.data.product_id,
    user:this.usr.id,
    data:JSON.stringify(compressed_data),
    width: this.canvas.width,
    height: this.canvas.height,
    urls:JSON.stringify(compressed_urls),
    design_title:this.title
  }

  this.api.saveDesigns(data).subscribe(
    res=>{
      Swal.fire(
        {
          icon:"success",
          text:"enregistrement reussi !!"
        }
      )
      console.log(res)
    },
    err=>{
      Swal.fire(
        {
          icon:"error",
          text:"une erreur s'est produite"
        }
      )
      console.log(err)
    }
  )
}else{
  let can=compress([this.canvas.toJSON()])
  let uri=compress([this.canvas.toDataURL(this.conf)])
   data={
    product:this.data.product_id,
    user:this.usr.id,
    data:JSON.stringify(can),
    width: this.canvas.width,
    height:this.canvas.height,
    urls:JSON.stringify(uri),
    design_title:this.title
  }
 

  this.api.saveDesigns(data).subscribe(
    res=>{
      console.log(res)
      Swal.fire(
        {
          icon:"success",
          text:"enregistrement reussi !!"
        }
      )
    },
    err=>{
      Swal.fire(
        {
          icon:"error",
          text:"une erreur s'est produite"
        }
      )
      console.log(err)
    }
  )
  
}
  
 

  }

  underline(){
    this.aladin.underline(this.canvas)
  }

  overline(){
    this.aladin.overline(this.canvas)
  }

  remove(){
    this.aladin.remove(this.canvas)
  }


  ngOnChanges(changes: SimpleChanges): void {
      
    
  }
}
