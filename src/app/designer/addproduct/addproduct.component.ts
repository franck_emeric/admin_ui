import { Component, Input, OnInit,OnChanges,SimpleChanges } from '@angular/core';
import Swal from 'sweetalert2';
import { ApiService, EventparserService } from 'src/app/core';
import compress from 'compress-base64';
@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})
export class AddproductComponent implements OnInit {
  
  hideSac=false
  showSac=false
  IsSac:any
  step1=true
  step11=true
  step2=false
  step3=false
  token="*"
  product_type:any={
    id:"",
    type:""
  }
  categories:any="Vetements"
  hideeditor=false
  data:any;
  file1:any
  file2:any
  mydata = {
    name:"pro",
    price:2550,
    promo : 5,
    dims:{
      M:true,
      L:true,
      XL:true,
      S:true,
      XXL:true,
      XXXL:true 
    }
  }
  url:any=[]
  product_category_label:any
  product_category_id:any
 
  @Input() list_category:any=[]
  @Input() list_typeProd:any=[]
  isCategory=false
  table_type:any=[]
  type:any=[]
  constructor(private Myevent:EventparserService,private api:ApiService) { }
  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.table_type = changes.list_typeProd.currentValue
  
  }
  ngOnInit(): void {
    if(this.step1){
      let tx1:any= document.getElementById('step1');
      tx1.style.background="#324161";
      tx1.style.color="white"
    }

    this.Myevent.iscategory.subscribe(
      res=>{
        var select:any= document.getElementById("type_pro");
        console.log(select)
        select.disabled=false;
        
      }      
    )
       
  }

  showstep2(){
    this.step2=true
    let tx:any= document.getElementById('step2')
    tx.style.background="#324161";
    tx.style.color="white"
  
    this.step1=false
    let tx1:any= document.getElementById('step1');
    tx1.style.background="green";
  }
  prevstep1(){
    this.step1=true
    this.step2=false
    let tx1:any= document.getElementById('step1');
       tx1.style.background="#324161";
       tx1.style.color="white"
    let tx:any= document.getElementById('step2')
    tx.style.background="green";
    tx.style.color="white"
  

  }
  showstep3(){
    this.step3=true
    this.step2=false
    let tx1:any= document.getElementById('step1');
       tx1.style.background="green";
       tx1.style.color="white"
    let tx:any= document.getElementById('step2')
    tx.style.background="green";
    tx.style.color="white"

    let tx3:any=document.getElementById('step3');
    tx3.style.background="#324161"
    tx3.style.color="white"

  }

  prevstep2(){
    this.step3=false
    this.step2=true
    let tx1:any= document.getElementById('step1');
       tx1.style.background="green";
       tx1.style.color="white"
    let tx:any= document.getElementById('step2')
    tx.style.background="#324161";
    tx.style.color="white"

    let tx3:any=document.getElementById('step3');
    tx3.style.background="green"
    tx3.style.color="white"
 
  }

  register(){
    var data={
      name:this.mydata.name,
      product_urls: this.url,
      type:this.product_type.id

    }

    data.product_urls=JSON.stringify(data.product_urls)

    if(data.type && data.name && this.url.length>0){
      this.api.saveProduct(data).subscribe(res=>{
        var resp:any=res
        if(resp.status){
          Swal.fire({
            title: 'Votre produit est bien enregistré ',
            text: "Je veux modeliser mon produit",
            icon: 'success',
            showCancelButton:true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui',
            cancelButtonText:'Non plus tard'
          }).then((result:any) => {
           
            if (result.isConfirmed) {

              this.data={
                type:this.product_type.type,
                product_urls:this.url,
                product_id: resp.data.insertId,
                category_label:this.product_category_label
              }

              this.Myevent.editor.next(
                {
                  default :true,
                  data: this.data
                } 
              )
              this.Myevent.productsaved.next(true)

            }else{
              this.prevstep1()
            }
          })
        }
      },
      err=>{
        console.log(err)
        Swal.fire(
          {
            icon:"error",
            text:"une erreur s'est produite"
          }
        )
      })

    }else{
      Swal.fire(
        {
          icon:"error",
          text:"renseignez tous les champs svp."
        }
      )
    }
 
  }

  getTyp_Cat(event:any){
    var item = event.target.value;
    if(item.indexOf('*') !=-1){  
      this.product_category_id=item.substr(0,item.indexOf('*'))
      this.product_category_label= item.substr(item.indexOf('*'))
  
      if(this.product_category_id.indexOf("*")!=-1){
        this.product_category_id=  this.product_category_id.substr(0,this.product_category_id.indexOf("*")-1)
      }

      if(this.product_category_label.indexOf("*")!= -1){
        this.product_category_label= this.product_category_label.substr(this.product_category_label.indexOf('*')+1)
      }
      this.Myevent.iscategory.next(this.product_category_label)
      console.log(this.product_category_label)

      var table:any= []
      for(let i=0; i<this.list_typeProd.length; i++){
        if(this.product_category_id != this.list_typeProd[i].type_category_id){
      
        }else{
          table.push(this.list_typeProd[i]);
        }
      
      }
      this.table_type=table;
            
    }
  }

  getType(event:any){
    var item = event.target.value;
    if(item.indexOf('*') !=-1){
      this.product_type.id=item.substr(0,item.indexOf('*'))
      this.product_type.type= item.substr(item.indexOf('*'))
      if(this.product_type.id.indexOf("*")!=-1){
        this.product_type.id=  this.product_type.id.substr(0,this.product_type.id.indexOf("*")-1)
      }
      if(this.product_type.type.indexOf("*")!= -1){
        this.product_type.type= this.product_type.type.substr(this.product_type.type.indexOf('*')+1)
      }
      this.Myevent.istype_prod.next(this.product_type.type)

      this.table_type=this.list_typeProd;
    }
  
  }



  upload(event:any){
    const files = event.target.files;
    const urls:any=[]
    const host = "http://localhost:8080/" 

    const formdata :any= new FormData();
    for (let i = 0; i < files.length; i++) {
      urls.push(host+files[i].name)
      formdata.append("file",files[i])

    }
    this.api.saveFile(formdata).subscribe(res=>{
      if(res.status=true){
        this.url=urls
      }
      console.log(res)
    },
    error=>{
      console.log(error)
    })
    
  }


  
}


