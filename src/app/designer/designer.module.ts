import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DesigndashComponent } from './designdash/designdash.component';
import { SharedModule } from '../shared';
import { DesignerRoutingModule } from './designer-routing.module';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { EditorComponent } from './editor/editor.component';
import { ProfileComponent } from './designerprofile/designerprofile.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { DesignerGuard } from '../core/auths';
import { FormoptionsComponent } from './formoptions/formoptions.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AddShapeComponent } from './add-shape/add-shape.component';


@NgModule({
  declarations: [
    DesigndashComponent,
    AddproductComponent,
    ProductlistComponent,
    EditorComponent,
    ProfileComponent,
    FormoptionsComponent,
    AddShapeComponent
  ],
  imports: [
    SharedModule,
    DesignerRoutingModule,
    ContextMenuModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  providers:[DesignerGuard]
})
export class DesignerModule { }
