import { Component, OnInit, Input, SimpleChanges, ElementRef, Output, EventEmitter} from '@angular/core';
import { EventparserService } from '../../core/communications/eventparser.service';
import { ApiService } from '../../core/httpservices/api.service';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-add-shape',
  templateUrl: './add-shape.component.html',
  styleUrls: ['./add-shape.component.scss']
})
export class AddShapeComponent implements OnInit {
  step1 = true
  token="*"
  data:any
  name:any
  choosen_type:any
  file1_4:any
  file2_4:any
  file3_4:any
  file4_4:any
  typeEl=["Background","shapes or cliparts","maquette personnalisable"]
  theme:any
  new_themetype_id:any
  new_themetype_label:any
  @Input() Listheme_type:any=[]
  @Input() usr:any;
  @Input() prod_type:any=[];
  shape:any
  can_reg=false;
  has_back=false;
  choice="choix";

  _type:any
  @Output() item=new EventEmitter<any>()

  url:any=[];
  constructor(private eve : EventparserService, private obs : ApiService,private el:ElementRef) { }
  
  ngOnInit(): void {

    if(this.step1){
      let tx1:any= document.getElementById('step1');
      tx1.style.background="#324161";
      tx1.style.color="white"
    }

    this.eve.isElement.subscribe(res=>{
      var clic:any = document.getElementById("el_name")
      clic.disabled=false;
    })

  }

saveShape(){

  var data= {
      name:this.name,
      type:this.shape,
      theme: this.choosen_type,
      url:JSON.stringify(this.url),
      user:this.usr.id,
      prod:this._type
  }
  if(data.name && data.theme  && data.url.length>0 && data.prod){

    if(this.shape==="maquette personnalisable"){
      data.type=this._type
      Object.assign(data,{product_urls:data.url})
      this.obs.saveProduct(data).subscribe(
        res=>{
          var resp:any=res
          Swal.fire({
            title: 'enregistrement reussi',
            icon: 'success',
            
          }).then((result:any)=>{
            if(result.isConfirmed){
              this.item.emit(resp.data)
              this.eve.themesaved.next(true)
            }
          })
        },
        err=>{
          console.log(err)
        }
      )
    }

    if(this.shape!="maquette personnalisable"){
      this.obs.saveItem(data).subscribe(
        res=>{
          console.log(res)
          var resp:any=res
          Swal.fire({
            title: 'Votre Thème a bien été enregistré ',
            icon: 'success',
            
          }).then((result:any)=>{
            if(result.isConfirmed){
              this.item.emit(resp.data)
              this.eve.themesaved.next(true)
            }
          })
        },
        err=>{
          console.log(err)
        }
      )
    }
   
  }else{
    Swal.fire(
      {
        icon:"error",
        text:"renseignez tous les champs svp."
      }
    )
  }


}


OntypeSelect(event:any){
  if(event.target.value!="choix"){
    this.shape=event.target.value
    if(this.shape.toLowerCase()==="background" || this.shape.toLowerCase()==="maquette personnalisable"){
      this.has_back=true;
    }else

    if (this.shape.toLowerCase()!="background" && this.shape.toLowerCase()!="maquette personnalisable") {
      this.has_back=false;

    }
  }else{
    this.has_back=false;

  }

}

gt_prod_type(event:any){
  var item = event.target.value
  var new_themetype_id:any;
  var new_themetype_label:any;
  if(item.indexOf('*') !=-1){  
    new_themetype_id = item.substr(0,item.indexOf('*'))
    new_themetype_label = item.substr(item.indexOf('*'))

    if(new_themetype_id.indexOf("*")!=-1){
      new_themetype_id=  new_themetype_id.substr(0,new_themetype_id.indexOf("*")-1)
    }
    
    if(new_themetype_label.indexOf("*")!= -1){
      new_themetype_label= new_themetype_label.substr(new_themetype_label.indexOf('*')+1)
    }
    this._type = new_themetype_id
  }
  
}

  Gettype_theme(event:any){
    var item = event.target.value
    if(item.indexOf('*') !=-1){  
      this.new_themetype_id = item.substr(0,item.indexOf('*'))
      this.new_themetype_label = item.substr(item.indexOf('*'))

      if(this.new_themetype_id.indexOf("*")!=-1){
        this.new_themetype_id=  this.new_themetype_id.substr(0,this.new_themetype_id.indexOf("*")-1)
      }
      
      if(this.new_themetype_label.indexOf("*")!= -1){
        this.new_themetype_label= this.new_themetype_label.substr(this.new_themetype_label.indexOf('*')+1)
      }

      this.choosen_type = this.new_themetype_id
      this.eve.isElement.next(this.new_themetype_label)
      
    }
    
  }

  
  register(){
    var data={
      theme:this.theme,
      user:this.usr.id
            
    }
    if(data.theme && data.user){
      this.obs.saveTheme(data).subscribe(res=>{
        var resp:any=res
        if(resp.status){
          this.Listheme_type=resp.data
          Swal.fire({
            title: 'Votre Thème a bien été enregistré ',
            icon: 'success',
            
          }).then((result:any)=>{
            if(result.isConfirmed){
              this.eve.themesaved.next(true)
            }
          })
        }
      },
      err=>{
        console.log(err)
        Swal.fire(
          {
            icon:"error",
            text:"une erreur s'est produite"
          }
        )
      })

    }else{
      Swal.fire(
        {
          icon:"error",
          text:"renseignez tous les champs svp."
        }
      )
    }
 
  }
  

  uploads(event:any){
    const files = event.target.files;
    const urls:any=[]
    const host = environment.baseImagePath
    const formdata :any= new FormData();
    for (let i = 0; i < files.length; i++) {
      urls.push(host+files[i].name)
      formdata.append("file",files[i])

    }

    this.obs.saveFile(formdata).subscribe(res=>{
      console.log(res)
      var resp=res
      if(resp.status){
        this.can_reg=true
        this.url=urls;
      }
    },
    error=>{
      console.log(error)
    })
    
  
  }

}
