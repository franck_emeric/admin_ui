import { Component, Input, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { EventparserService } from 'src/app/core';
@Component({
  selector: 'app-formoptions',
  templateUrl: './formoptions.component.html',
  styleUrls: ['./formoptions.component.scss']
})
export class FormoptionsComponent implements OnInit {
  cloth_size:any=[];
  cloth_printing:any=[];
  pack_options_size:any;
  pack_options_color:any;
  bache_dim_options:any
  cloth_options_size:any
  cloth_options_p:any;
  kakemono_sup_options:any
  vinyle_type_options:any
  micro_perf_options:any
  tasse_options:any
  stylo_options:any
  stylo_impr_options:any
  porte_cle_options:any
  carnet_format_options:any
  
  
  pack_size:any=[];
  pack_color:any=[];
  bache_dim:any=[];
  kakemono_support:any=[];
  vinyle_type:any=[];
  micro_perf:any=[];
  tasse_type:any=[];
  stylo_type:any=[];
  porte_cle_type:any=[];
  stylo_impr_type:any=[];
  carnet_format_type:any=[];
  
  pack=false;
  cloth=false;
  gadget=false;
  print=false;
  display=false;
  disp_bache=false;
  disp_vinyle=false;
  disp_kakemono=false;
  disp_micro_perf=false;
  gadg_tasse=false;
  gadg_stylo=false;
  gadg_porte_cle=false;
  print_carnet=false;
  print_etiquet=false;

  dropdownSettings :IDropdownSettings= {};

  constructor(private ev:EventparserService) { }

  ngOnInit(): void {

    

    this.ev.iscategory.subscribe(
      res=>{
        console.log(res)
           if(res){
            if(res==="vetements"){
              this.cloth=true
              this.pack=false
              this.gadget=false

 
             }else
         
             if(res==="emballages"){
               this.pack=true
               this.cloth=false
               this.gadget=false

         
              }else

              if(res==="gadgets"){
                this.pack=false
                this.cloth=false
                 this.gadget=true
               }else{
                this.pack=false
                this.cloth=false
                this.gadget=false
               }


           }
          }
      
    );
    this.ev.istype_prod.subscribe(
      res=>{
        console.log(res)
        if(res){
  
          if(res==="carnet"){
            this.print_carnet=true
            this.print_etiquet=false
            this.gadg_tasse=false
            this.disp_bache=false
            this.disp_micro_perf=false
            this.disp_vinyle=false
            this.disp_kakemono=false
          }else
  
          if(res==="etiquette"){
            this.print_etiquet=true
            this.print_carnet=false
            this.gadg_tasse=false
            this.disp_bache=false
            this.disp_micro_perf=false
            this.disp_vinyle=false
            this.disp_kakemono=false
          }else
  
          if(res==="Tasse"){
            this.gadg_tasse=true
            this.disp_bache=false
            this.disp_micro_perf=false
            this.disp_vinyle=false
            this.disp_kakemono=false
          }else
  
          if(res==="stylo"){
            this.gadg_stylo=true
            this.gadg_tasse=false
            this.disp_bache=false
            this.disp_micro_perf=false
            this.disp_vinyle=false
            this.disp_kakemono=false
          }else
          
          if(res==="bache"){
            this.disp_bache=true
            this.gadg_tasse=false
            this.gadg_stylo=false
            this.disp_micro_perf=false
            this.disp_vinyle=false
            this.disp_kakemono=false
          }else
  
          if(res==="vinyle"){
            this.disp_vinyle=true
            this.gadg_tasse=false
            this.gadg_stylo=false
            this.disp_kakemono=false
            this.disp_bache=false
            this.disp_micro_perf=false
          }else
  
          if(res==="kakemono"){
            this.disp_kakemono=true
            this.gadg_tasse=false
            this.gadg_stylo=false
            this.disp_bache=false
            this.disp_micro_perf=false
            this.disp_vinyle=false
          }else
  
          if(res==="micro perfore"){
            this.disp_micro_perf=true
            this.gadg_tasse=false
            this.gadg_stylo=false
            this.disp_vinyle=false
            this.disp_kakemono=false
            this.disp_bache=false
  
          }
          else{
            this.gadg_tasse=false
            this.gadg_stylo=false
            this.disp_kakemono=false
            this.disp_bache=false
            this.disp_micro_perf=false
            this.disp_vinyle=false
            
          }
        }
      }
    )

    this.cloth_size = [
    { item_id: 1, item_text: "S" },
    { item_id: 2, item_text: 'M' },
    { item_id: 3, item_text: 'L' },
    { item_id: 4, item_text: 'XL' },
    { item_id: 5, item_text: 'XXL' },

  ];

  this.tasse_type= [
    { item_id: 1, item_text: 'Tasse magique'},
    { item_id: 2, item_text: "Tasse  ordinaire" },
  ];

  this.stylo_type= [
    { item_id: 1, item_text: 'Stylo Grand Public'},
    { item_id: 2, item_text: "Stylo Semi VIP" },
    { item_id: 3, item_text: "Stylo VIP"},
  ];

  this.stylo_impr_type = [
    { item_id: 1, item_text: 'Serigraphie'},
    { item_id: 2, item_text: "Transfert" },
    { item_id: 3, item_text: "UV"},
  ];

  this.carnet_format_type = [
    { item_id: 1, item_text: 'A4'},
    { item_id: 2, item_text: "A5" },
    { item_id: 3, item_text: "A6"},
  ];
  /*this.pack_size = [
    { item_id: 1, item_text: "15/20" },
    { item_id: 2, item_text: '20/25' },
    { item_id: 3, item_text: '25/30' },
  ];*/



  this.pack_color = [
    { item_id: 1, item_text: 1 },
    { item_id: 2, item_text: 2 },
    { item_id: 3, item_text: 3 },
    { item_id: 4 , item_text: 4 },
  ];

  this.pack_size = [
    { item_id: 1, item_text: "15/20" },
    { item_id: 2, item_text: '20/25' },
    { item_id: 3, item_text: '25/30' },
    { item_id: 4, item_text: '28/35' },
    { item_id: 5, item_text: '35/45' },
    { item_id: 6, item_text: '45/50' },
    { item_id: 7, item_text: '50/55' },
  ];

  this.bache_dim = [
    { item_id: 1, item_text: "Supérieur" },
    { item_id: 2, item_text: "Standard" },
    
  ];

  this.kakemono_support = [
    { item_id: 1, item_text: "Petit Bas Supérieur" },
    { item_id: 2, item_text: "Large Bas Supérieur" },
    
  ];

  this.vinyle_type = [
    { item_id: 1, item_text: "Vinyle Opaque" },
    { item_id: 2, item_text: "Vinyle Transparent" },
    
  ];

  this.micro_perf = [
    { item_id: 1, item_text: "Supérieur" },
    { item_id: 2, item_text: "Standard" },
    
  ];

  this.cloth_printing = [
    { item_id: 1, item_text: "Broderie" },
    { item_id: 2, item_text: 'Flexographie' },
    { item_id: 3, item_text: 'Sublimation' },
    { item_id: 4, item_text: 'Serigraphie' },
    { item_id: 5, item_text: 'Transfert' },

  ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'tout cocher',
      unSelectAllText: 'tout decocher',
      itemsShowLimit: 3,
    };
  }

  onItemSelect(item: any) {
    console.log(item);
  }

  onSelectAll(item: any) {
    console.log(item);
  }

}
