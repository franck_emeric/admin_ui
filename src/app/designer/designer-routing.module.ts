import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DesignerGuard } from '../core/auths';
import { AthAdminGuard } from '../core/auths/guards/auth-admin';
import {DesigndashComponent } from './designdash/designdash.component';

const routes: Routes = [
  {path:"",component:DesigndashComponent,canActivate:[DesignerGuard]},
  {path:"me",component:DesigndashComponent,canLoad:[AthAdminGuard]}
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DesignerRoutingModule { }
