import { Component, Input, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core';
@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductlistComponent implements OnInit {
@Input() list:any
  data:any=[]
  constructor(private EVT:EventparserService) { }
  ngOnInit(): void {
   
  }
  letedit(data:any){
  this.EVT.editor.next(
    {
      default:true,
      data: Object.assign(data,{list:true}),

    }
  )
  }

 update(){

 } 
}
