import { Component, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core/communications'; 
import { ApiService } from 'src/app/core';
import { TokenStorageServiceService } from 'src/app/core/auths';
@Component({
  selector: 'app-designdash',
  templateUrl: './designdash.component.html',
  styleUrls: ['./designdash.component.scss']
})
export class DesigndashComponent implements OnInit {
 menu =
  {
   name:"designer", 
   user:"dave",
   role:"web dev"
 }
user:any
 typ_theme:any
 category:any;
 type:any
 type_pack:any;
 type_print:any;
 type_disp:any;
type_gadget:any;
data:any
list_of_product:any=[]
 design=false;
 listp=false;
 profile=false;
 update_product=false
 cloth=false
 pack=false 
 gadget=false
 print=false
 disp=false
 default=true;
  editor_item:any=[];
  list_of_models:any=[]
  constructor(private subject:EventparserService,private api:ApiService,private tokenservice:TokenStorageServiceService) { }
  ngOnInit(): void {

    this.getproduct()
    this.getCategories()
    this.getType()
    this.getmodel()

    setTimeout(() => {
      this.get_typTheme()
      this.gtItem()

    }, 1000);
    
    this.subject.design.subscribe(
      res=>{
        this.design=res;
        this.listp=false
        this.profile=false;
        this.update_product=false
        this.cloth=false
        this.pack=false
        this.gadget=false
        this.print=false
        this.disp=false
        this.default=false
      }
    )

    this.subject.productlist.subscribe(menu=>{
      this.listp=menu;
      this.profile=false;
      this.update_product=false
      this.cloth=false
      this.pack=false
      this.gadget=false
      this.print=false
      this.disp=false
      this.default=false
      this.design=false
    })
 
    this.subject.profile.subscribe(menu=>{
      this.listp=false;
      this.profile=menu;
      this.update_product=false
      this.cloth=false
      this.pack=false
      this.gadget=false
      this.print=false
      this.disp=false
      this.default=false
      this.design=false

    })

    this.subject.update.subscribe(menu=>{
      this.listp=false;
      this.profile=false;
      this.update_product=menu
      this.cloth=false
      this.pack=false
      this.gadget=false
      this.print=false
      this.disp=false
      this.default=false
      this.design=false


    })

    this.subject.clothes.subscribe(menu=>{
      this.listp=false;
      this.profile=false;
      this.update_product=false
      this.cloth=menu
      this.pack=false
      this.gadget=false
      this.print=false
      this.disp=false
      this.default=false
      this.design=false


    })

   

    this.subject.logout.subscribe(msg=>{
      if(msg){
        this.tokenservice.signOut()
        setTimeout(()=>{
          location.href="/login"
        },1000)
   
      }
    })

    this.subject.editor.subscribe(data=>{
      if(data.default){
        this.data=data.data;
        this.default=data.default;
        this.listp=false;
        this.profile=false;
        this.update_product=false
        this.cloth=false
        this.pack=false
        this.gadget=false
        this.print=false
        this.disp=false
        this.design=false

      }else{
        console.log(data)
      }
    })

    this.tokenservice.getUser().then(
      (user)=>{
        console.log(user)
        if(user)
        this.menu.user=user.name;

        this.menu.role=user.role

        this.user=user
      }
    ).catch((err)=>{
      console.log(err)
    })
  

    this.subject.productsaved.subscribe(message=>{
      if(message){
        this.getproduct()
      }
    })

    this.subject.themesaved.subscribe(res=>{
      if(res){

      }
    })
  }

  get_typTheme(){
    this.api.getType_theme(this.user.id).subscribe(res=>{
      var result:any = res;
      console.log(res)
      if(result.status){
        this.typ_theme = result.data
      }
      console.log(result)
    })
  }
 gtItem(){
  this.api.geTItem(this.user.id).subscribe(
   res=>{
    var resp:any=res
   if(resp.status){
    this.editor_item = resp.data;
    console.log(this.editor_item,123) 
   }  
   } 
  )
 } 

  getCategories(){
    this.api.getAllCategories().subscribe(res=>{
      var data:any = res
      if(data.status){
        this.category = data.data

      }
      console.log(data)
      
    },
    err=>{
      console.log(err)
    }
    )
  }

  getType(){
    this.api.getProductType().subscribe(res=>{
      var dat_type:any = res
      if(dat_type.status){
        this.type = dat_type.data
      }
      console.log(dat_type)


    },
    err=>{
      console.log(err)
    }
    )
  }

  getproduct(){
    this.api.getProducts("product").subscribe(res=>{
      var resp:any=res
      if(resp.status){
        for(let item of resp.data){
          item.product_urls= JSON.parse(item.product_urls)
          this.list_of_product.push(item)
        }
        console.log(this.list_of_product)

      }

    },
    err=>{
      console.log(err)
    })
  }


  getmodel(){
    this.api.getProducts("maquette").subscribe(res=>{
      var resp:any=res
      if(resp.status){
        for(let item of resp.data){
          item.product_urls= JSON.parse(item.product_urls)
          this.list_of_models.push(item)
        }
        console.log(this.list_of_models,52)


      }

    },
    err=>{
      console.log(err)
    })
  }

  update(data:any){
    this.editor_item=data
  }

}
