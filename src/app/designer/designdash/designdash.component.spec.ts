import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesigndashComponent } from './designdash.component';

describe('DesigndashComponent', () => {
  let component: DesigndashComponent;
  let fixture: ComponentFixture<DesigndashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesigndashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesigndashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
