import { Component, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core';

@Component({
  selector: 'app-delivery-menu',
  templateUrl: './delivery-menu.component.html',
  styleUrls: ['./delivery-menu.component.scss']
})
export class DeliveryMenuComponent implements OnInit {

  constructor(private mevent: EventparserService) { }

  ngOnInit(): void {

  }
showorderpacks(){
  this.mevent.pack_order.next(true)
}
showdelivery(){
  this.mevent.end_pack_order.next(true)
}
showdelivered(){
  this.mevent.delevered.next(true)
}
showprofile(){
  this.mevent.profiles.next(true)
}
}
