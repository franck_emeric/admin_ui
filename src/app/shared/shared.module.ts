import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule } from '@angular/forms';
import { DesignermenuComponent } from './designermenu/designermenu.component';
import { ProdmenuComponent } from './prodmenu/prodmenu.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { PreloadComponent } from './preload/preload.component';
import {ColorCircleModule} from 'ngx-color/circle';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { CustomersmenuComponent } from './customersmenu/customersmenu.component';
import { DeliveryMenuComponent } from './delivery-menu/delivery-menu.component';

@NgModule({
  declarations: [
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    DesignermenuComponent,
    ProdmenuComponent,
    UserprofileComponent,
    PreloadComponent,
    AdminMenuComponent,
    CustomersmenuComponent,
    DeliveryMenuComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ColorCircleModule,

  ],
  exports:[
    
    SidebarComponent,HeaderComponent,
    FooterComponent,FormsModule,
    CommonModule,DesignermenuComponent,
    ProdmenuComponent,UserprofileComponent,
    PreloadComponent,ColorCircleModule,

  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
