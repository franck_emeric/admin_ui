import { Component, Input, OnInit } from '@angular/core';
import { TokenStorageServiceService } from 'src/app/core/auths';
@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.scss']
})
export class UserprofileComponent implements OnInit {
  photo=true
  viewimage:any
  newphoto=false
  user:any
  constructor(private tokenservice:TokenStorageServiceService) { }

  ngOnInit(): void {
  this.tokenservice.getUser().then((user)=>{
  this.user=user;
  })
  }


 Onchange(event:any){
   let file= event.target.files[0]

   var reader= new FileReader();
   reader.onload=()=>{
    this.viewimage=reader.result;
    this.photo=false
    this.newphoto=true
   };
   reader.readAsDataURL(file)
 }


}
