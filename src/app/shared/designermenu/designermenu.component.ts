import { Component, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core/communications';
@Component({
  selector: 'app-designermenu',
  templateUrl: './designermenu.component.html',
  styleUrls: ['./designermenu.component.scss']
})
export class DesignermenuComponent implements OnInit {

  constructor(private subject:EventparserService) { }

  ngOnInit(): void {
  }

  profiles(){
    this.subject.profile.next(true)

  }

  Update(){
    this.subject.update.next(true)

  }

  Design(){
    this.subject.design.next(true)
  }
  
  productlist(){
    this.subject.productlist.next(true)

  }

  display(){
    this.subject.display.next(true)

  } 

  printed(){
    this.subject.printed.next(true)

  }

  gadget(){
    this.subject.gadget.next(true)

  } 

  packages(){
    this.subject.packages.next(true)

  }

  clothes(){
    this.subject.clothes.next(true)

  }

  cachebody(){
    this.subject.designdefault.next(true)

  }
 
 logout(){
 // location.href ="/"
 this.subject.logout.next(true)
 }

 custom_design(){
   this.subject.custom_design.next(true)

 }

}
