import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignermenuComponent } from './designermenu.component';

describe('DesignermenuComponent', () => {
  let component: DesignermenuComponent;
  let fixture: ComponentFixture<DesignermenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignermenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignermenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
