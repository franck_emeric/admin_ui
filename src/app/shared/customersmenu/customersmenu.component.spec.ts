import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersmenuComponent } from './customersmenu.component';

describe('CustomersmenuComponent', () => {
  let component: CustomersmenuComponent;
  let fixture: ComponentFixture<CustomersmenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomersmenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
