import { Component, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core';
import { TokenStorageServiceService } from 'src/app/core/auths';

@Component({
  selector: 'app-customersmenu',
  templateUrl: './customersmenu.component.html',
  styleUrls: ['./customersmenu.component.scss']
})
export class CustomersmenuComponent implements OnInit {

  constructor(private Event:EventparserService) { }

  ngOnInit(): void {
  }
 neworder(){
   this.Event.Neworders.next(true)
 }
 pendorder(){
  this.Event.pendorders.next(true)
}
delivery_order(){
  this.Event.deliveryorders.next(true)
}
order_delivered(){
  this.Event.orderdelivered.next(true)
}
order_packs(){
  this.Event.orderpacks.next(true)
}
order_cancel(){
  this.Event.ordercancel.next(true)
}
bodymain(){
  this.Event.Bodymain.next(true)
}
profile(){
  this.Event.profile_customer.next(true)
}
logOut(){
this.Event.logout.next(true)
}
}
