import { Component, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core';
@Component({
  selector: 'app-prodmenu',
  templateUrl: './prodmenu.component.html',
  styleUrls: ['./prodmenu.component.scss']
})
export class ProdmenuComponent implements OnInit {
  profile=false
  neworders=false
  endorders=false
  constructor(private Myevent:EventparserService) { }

  ngOnInit(): void {
  }
  
    showprofile(){
      this.Myevent.profile.next(true)
      }
    shownewordres(){
        this.Myevent.new_orders.next(true)
      }
      showendorders(){
       this.Myevent.producted_item.next(true)
        }
      showpending(){
        this.Myevent.pending.next(true)
      }

      logout(){
          this.Myevent.logout.next(true)
      }

}
