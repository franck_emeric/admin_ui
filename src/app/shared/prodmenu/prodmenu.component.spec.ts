import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdmenuComponent } from './prodmenu.component';

describe('ProdmenuComponent', () => {
  let component: ProdmenuComponent;
  let fixture: ComponentFixture<ProdmenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProdmenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
