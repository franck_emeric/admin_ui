import { Component, Input, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
@Input() data:any
production=false;
designer=false;
  constructor(private Event:EventparserService) { }

  ngOnInit(): void {
    if(this.data.name=="production"){
      this.production=true
    }
    if(this.data.name=="designer"){
      this.designer=true
    }
  }

 profle(){
   this.Event.profile.next(true)
 } 

 listofprod(){
  this.Event.productlist.next(true)
 }

 logout(){
   this.Event.logout.next(true)
 }

 orders(){
   this.Event.new_orders.next(true)
 }
}
