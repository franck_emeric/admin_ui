import { Component, OnInit } from '@angular/core';
import { EventparserService } from 'src/app/core';
@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss']
})
export class AdminMenuComponent implements OnInit {
 
  constructor(private myev:EventparserService) { }

  ngOnInit(): void {
  }



  add(){
    this.myev.add_pro.next(true)
  }

 update(){
  this.myev.update_pro.next(true)
 } 

  showorders(){
  this.myev.new_orders.next(true)
  }
  showcreateusers(){
   this.myev.add_user.next(true)
  }
  showprofile(){
  this.myev.profile.next(true)
  }
 showlist_user(){
   this.myev.list_user.next(true)
 }
 showpartner_order(){
   this.myev.Order_partner.next(true)
 }
 showpartner(){
   this.myev.Partner.next(true)
 }
 showpartner_prod(){
   this.myev.Product_partner.next(true)
 }
 showapprov_partner(){
   this.myev.Aprov_partner.next(true)
 }

 logout(){
  this.myev.logout.next(true)

 }
}
