import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
@Input() menu:any
  constructor() { }
 designer=false
 production=false
 admin=false
 customer=false
 delivery=false
  ngOnInit(): void {
    if(this.menu.name=="designer"){
     this.designer=!this.designer
    }
    if(this.menu.name=="production"){
      this.production=!this.production
     }
     if(this.menu.name=="customerservice"){
      this.customer=!this.customer
     }

     if(this.menu.name=="me"){
      this.admin=!this.admin
     }
     if(this.menu.name=="packservice"){
     this.delivery=!this.delivery
     }
     
  }
 

}
