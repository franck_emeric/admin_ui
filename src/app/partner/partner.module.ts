import { NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { PartnerRoutingModule } from './partner-routing.module';
import { SharedModule } from '../shared';
import { PartnerdashComponent } from './partnerdash/partnerdash.component';
import { DefaultpartComponent } from './defaultpart/defaultpart.component';



@NgModule({
  declarations: [
    PartnerdashComponent,
    DefaultpartComponent
  ],
  imports: [
    SharedModule,
    PartnerRoutingModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class PartnerModule { }
