import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PartnerGuard } from '../core/auths/guards/partner/partner.guard';
import { PartnerdashComponent } from './partnerdash/partnerdash.component';

const routes: Routes = [
  {path:"",component:PartnerdashComponent,canActivate:[PartnerGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PartnerRoutingModule { }
