import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarketingdashComponent } from './marketingdash/marketingdash.component';

const routes: Routes = [
  {path:"",component:MarketingdashComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MarketingRoutingModule { }
