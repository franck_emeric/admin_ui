import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketingdashComponent } from './marketingdash.component';

describe('MarketingdashComponent', () => {
  let component: MarketingdashComponent;
  let fixture: ComponentFixture<MarketingdashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketingdashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketingdashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
