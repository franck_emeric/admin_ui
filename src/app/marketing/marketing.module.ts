import { NgModule } from '@angular/core';
import { MarketingdashComponent } from './marketingdash/marketingdash.component';
import { MarketingRoutingModule } from './marketing-routing.module';
import { SharedModule } from '../shared';



@NgModule({
  declarations: [
    MarketingdashComponent
  ],
  imports: [
    SharedModule,
    MarketingRoutingModule
  ]
})
export class MarketingModule { }
