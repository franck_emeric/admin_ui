import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { AdminGuard } from '../core/auths/guards/admin/admin.guard';
//import { AthAdminGuard } from '../core/auths/guards/auth-admin';
import { AdmindashComponent } from './admindash/admindash.component';

const routes: Routes = [

  {path:"",component:AdmindashComponent,//canActivate:[AdminGuard]
},
  //{path:"",component:AdmindashComponent}//canActivate:[AdminGuard],canLoad:[AthAdminGuard]}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
