import { NgModule } from '@angular/core';
import { AdmindashComponent } from './admindash/admindash.component';
import { DefaultComponent } from './default/default.component';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared';
import { CreateUserComponent } from './create-user/create-user.component';
import { ListUserComponent } from './list-user/list-user.component';
import { OdersOverviewComponent } from './oders-overview/oders-overview.component';
import { MeComponent } from './me/me.component';
import { ApprouvePartnerComponent } from './approuve-partner/approuve-partner.component';
import { PartnerComponent } from './partner/partner.component';
import { OrdersPartnerComponent } from './orders-partner/orders-partner.component';
import { PartnerProductsComponent } from './partner-products/partner-products.component';
import { AddProdInfoComponent } from './add-prod-info/add-prod-info.component';
import { UpdateProdInfoComponent } from './update-prod-info/update-prod-info.component';

@NgModule({
  declarations: [
    AdmindashComponent,
    DefaultComponent,
    CreateUserComponent,
    ListUserComponent,
    OdersOverviewComponent,
    MeComponent,
    ApprouvePartnerComponent,
    PartnerComponent,
    OrdersPartnerComponent,
    PartnerProductsComponent,
    AddProdInfoComponent,
    UpdateProdInfoComponent
  ],
  imports: [
    SharedModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
