import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OdersOverviewComponent } from './oders-overview.component';

describe('OdersOverviewComponent', () => {
  let component: OdersOverviewComponent;
  let fixture: ComponentFixture<OdersOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OdersOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OdersOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
