import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProdInfoComponent } from './add-prod-info.component';

describe('AddProdInfoComponent', () => {
  let component: AddProdInfoComponent;
  let fixture: ComponentFixture<AddProdInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddProdInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProdInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
