import { Component, OnInit,Input } from '@angular/core';
import { ApiService } from 'src/app/core';
import  Swal  from 'sweetalert2'
@Component({
  selector: 'app-add-prod-info',
  templateUrl: './add-prod-info.component.html',
  styleUrls: ['./add-prod-info.component.scss']

})
export class AddProdInfoComponent implements OnInit {
categorie:any
@Input() Categories:any=[]
@Input() Types:any=[]
data:any
label:any
type:any
color_price:any
price:any
printing_label:any
printing_type:any
format_width:any
format_height:any
format_price:any
format_other:any
mode_price:any
mode_size:any
mode_label:any

  constructor(private http:ApiService) { }

  ngOnInit(): void {
  
  }


Add_category(){
 var data:any={
   label:this.categorie,
   user:35
 }
 this.http.Add_category(data).subscribe(res=>{
   console.log(res)
   var data:any=res
   if(data.status==true){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Catégories bien enregistré',
      showConfirmButton: false,
      timer: 1500
    })
   }else{
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: "Une erreur s'est produite",
      showConfirmButton: false,
      timer: 1500
    })
   }
 }, err=>{
   console.log(err)
 })
}
Onchange(event:any){
 
 this.label=event.target.value
 
 console.log(this.label)
}
onchange2(event:any){
  this.printing_type=event.target.value
  console.log(this.printing_type)
}
add_type(){  
  let data:any={
    label:this.type,
    category:this.label
  }
this.http.Add_Type(data).subscribe(res=>{
  console.log(res)
  var data:any=res
   if(data.status==true){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Enregistré',
      showConfirmButton: false,
      timer: 1500
    })
   }else{
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: "Une erreur s'est produite",
      showConfirmButton: false,
      timer: 1500
    })
   }
},err=>{
  console.log(err)
})
}

ad_printing_type(){
  var data:any={
    label:this.printing_label,
    printing_price:this.price,
    color_price:this.color_price,
    type: this.printing_type
  }
  console.log(data)
  this.http.Add_printing_type(data).subscribe(res=>{
    console.log(res)
    var data:any=res
   if(data.status==true){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Enregistré',
      showConfirmButton: false,
      timer: 1500
    })
   }else{
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: "Une erreur s'est produite",
      showConfirmButton: false,
      timer: 1500
    })
   }
  },err=>{
    console.log(err)
  })
}

ad_format(){
  var data:any={
    width:this.format_width,
    height:this.format_height,
    type:this.printing_type,
    price:this.format_price,
    other:this.format_other

  }
  console.log(data)
  this.http.Add_format(data).subscribe(res=>{
    console.log(res)
    var data:any=res
   if(data.status==true){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Enregistré',
      showConfirmButton: false,
      timer: 1500
    })
   }else{
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: "Une erreur s'est produite",
      showConfirmButton: false,
      timer: 1500
    })
   }
  },err=>{
    console.log(err)
  })
}

add_mode(){
  var data:any={ 
    label:this.mode_label,
    type:this.printing_type,
    price:this.mode_price,
    size:this.mode_size

  }
  console.log(data)
  this.http.Add_mode(data).subscribe(res=>{
    console.log(res)
    var data:any=res
   if(data.status==true){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Enregistré',
      showConfirmButton: false,
      timer: 1500
    })
   }else{
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: "Une erreur s'est produite",
      showConfirmButton: false,
      timer: 1500
    })
   }
  },err=>{
    console.log(err)
  })
}

}
