import { Component, OnInit } from '@angular/core';
import { ApiService, EventparserService } from 'src/app/core';
@Component({
  selector: 'app-admindash',
  templateUrl: './admindash.component.html',
  styleUrls: ['./admindash.component.scss']
})
export class AdmindashComponent implements OnInit {
default=true;
create=false;
me=false
order=false
list_user=false
partner=false
aprv_partner=false
order_partner=false
partner_prod=false
update_pro=false;
addpro=false;
Orders:any=[]
menu={
  name:"me",
  user:"Dave",
  role:"software Engineer"
}
Categories:any=[]
Type:any=[]
constructor(private EVENT:EventparserService, private http:ApiService) { }

  ngOnInit(): void {
  this.http.getOrders().subscribe(res=>{
    console.log(res)
    let data:any=res
    if(data.status){
      this.Orders=data.data
    }
  })
  this.http.getAllCategories().subscribe(res=>{
    console.log(res)
    var data= res
    if(data.status){
      this.Categories=data.data
    }
  },err=>{
    console.log(err)
  })
  this.http.getProductType().subscribe(res=>{
    console.log(res)
    var data= res
    if(data.status){
      this.Type=data.data
    }
  },err=>{
    console.log(err)
  })
    this.EVENT.update_pro.subscribe(
      menu=>{
        this.update_pro=menu;
        this.create=false;
        this.default=false;
        this.me=false
        this.list_user=false;
        this.order=false
        this.partner=false
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false 
        this.addpro=false;
 
      }
    )

    this.EVENT.add_pro.subscribe(
      menu=>{
       this.addpro=menu;
       this.update_pro=false;
       this.create=false;
       this.default=false;
       this.me=false
       this.list_user=false;
       this.order=false
       this.partner=false
       this.aprv_partner=false
       this.order_partner=false
       this.partner_prod=false 

      }
    )
    this.EVENT.add_user.subscribe(
      menu=>{
        this.update_pro=false;
        this.addpro=false;
        this.create=menu;
        this.default=false;
        this.me=false
        this.list_user=false;
        this.order=false
        this.partner=false
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
      }
    )

    this.EVENT.new_orders.subscribe(
      menu=>{
        this.update_pro=false;
        this.addpro=false;

        this.default=false;
        this.create=false;
          this.me=false;
          this.list_user=false;
          this.order=menu
          this.partner=false
          this.aprv_partner=false
          this.order_partner=false
          this.partner_prod=false
      }
    )

    this.EVENT.profile.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=menu;
        this.order=false
        this.partner=false
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
    })

    this.EVENT.list_user.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=menu;
        this.me=false;
        this.order=false 
        this.partner=false
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
    })
    this.EVENT.Partner.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=menu;
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=false
    })
    this.EVENT.Product_partner.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false; 
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=false;
        this.aprv_partner=false
        this.order_partner=false
        this.partner_prod=menu;
    })
    this.EVENT.Aprov_partner.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=false;
        this.aprv_partner=menu
        this.order_partner=false
        this.partner_prod=false;
    })
    this.EVENT.Order_partner.subscribe(menu=>{
      this.update_pro=false;
      this.addpro=false;
      this.default=false;
      this.create=false;
      this.list_user=false;
        this.me=false;
        this.order=false;
        this.partner=false;
        this.aprv_partner=false
        this.order_partner=menu;
        this.partner_prod=false;
    })

    this.EVENT.logout.subscribe(msg=>{
      if(msg){
       // this.tokenservice.signOut()
        setTimeout(()=>{
          location.href="/login"
        },1000)
   
      }
    })
  }


}
