import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprouvePartnerComponent } from './approuve-partner.component';

describe('ApprouvePartnerComponent', () => {
  let component: ApprouvePartnerComponent;
  let fixture: ComponentFixture<ApprouvePartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApprouvePartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprouvePartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
