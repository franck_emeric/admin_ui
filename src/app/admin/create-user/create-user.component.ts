import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  role=["designer","production","customer_service","admin"];
  roles:any
  nom:any
  prenom:any
  telephone:any
  email:any
  perm:any
  perms=["single","full"]
  constructor(private api:ApiService) { }

  ngOnInit(): void {
  }
  onchange(event:any){
    if(event.target.value== this.role[0]){
      this.roles= this.role[0]
    
    }
    if(event.target.value== this.role[1]){
      this.roles= this.role[1]
    
    }
    if(event.target.value== this.role[2]){
      this.roles= this.role[2]
    
    }
    if(event.target.value== this.role[3]){
      this.roles= this.role[3]
    
    }
    if(event.target.value== this.role[4]){
      this.roles= this.role[4]
    
    }
    console.log(this.roles)
    }

    getperm=(event:any)=>{
    var perm = event.target.value

      if(perm=="single") {
        this.perm=1
      }
      if(perm=="full")
      {
        this.perm=2
      }
    }

   register(){
    var data={
      email:this.email,
      role:this.roles,
      url:"http://localhost:4200/register",
      perm:this.perm
    }

    if(this.perm && this.role){
      this.api.inviteUser(data).subscribe(res=>{
        console.log(res)
      },
      err=>{
        console.log(err)
      }
 
      )
    }
    console.log(data)

   
   } 
}
