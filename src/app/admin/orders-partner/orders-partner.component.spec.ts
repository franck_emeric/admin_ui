import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersPartnerComponent } from './orders-partner.component';

describe('OrdersPartnerComponent', () => {
  let component: OrdersPartnerComponent;
  let fixture: ComponentFixture<OrdersPartnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdersPartnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersPartnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
