import { Component, OnInit, Input,OnChanges } from '@angular/core';

@Component({
  selector: 'app-order-delivered',
  templateUrl: './order-delivered.component.html',
  styleUrls: ['./order-delivered.component.scss']
})
export class OrderDeliveredComponent implements OnInit,OnChanges {
@Input() delivered:any=[]
element:any=[]
search:any
  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(): void {
    this.element=this.delivered 
  }
  inputSearch(){
    if(this.search){
      this.element=this.delivered.filter((product:any)=>{
        if(product.userorder_id.toString().includes(this.search.toString()) ||
        product.user_first_name.toLowerCase().includes(this.search.toLowerCase())){
          return product
        } 
      })
    }else{
      this.element=this.delivered
    }
  }
}
