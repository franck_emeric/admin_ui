import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AthAdminGuard } from '../core/auths/guards/auth-admin';
import { CustomerServiceGuard } from '../core/auths/guards/customer-service/customer-service.guard';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {path:"",component:DashboardComponent,//canActivate:[CustomerServiceGuard]
},

  {path:"",component:DashboardComponent,canLoad:[AthAdminGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerServiceRoutingModule { }
