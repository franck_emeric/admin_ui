import { Component, OnInit } from '@angular/core';
import { ApiService, EventparserService } from 'src/app/core';
import { TokenStorageServiceService } from 'src/app/core/auths';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  neworder=true;
  pendorder=false
  delivery_order=false
  order_delivered=false
  order_cancel=false
  order_packs=false
  bodymain=false
  profile=false
  new:any=[]
  pending:any=[]
  packetages:any=[]
  delivery:any=[]
  delivered:any=[]
  menu={
    name:"customerservice",
    user:"Dave",
    role:"Services"
  }
  constructor(private EVENT:EventparserService,private tokenService:TokenStorageServiceService,private api:ApiService) { }

  ngOnInit(): void {
    
    this.EVENT.Bodymain.subscribe(menu=>{
      this.neworder=false;
      this.pendorder=false
      this.delivery_order=false
      this.order_delivered=false
      this.order_cancel=false
      this.order_packs=false
      this.bodymain=menu
      this.profile=false
    })
    this.EVENT.profile_customer.subscribe(menu=>{
      this.neworder=false;
      this.pendorder=false
      this.delivery_order=false
      this.order_delivered=false
      this.order_cancel=false
      this.order_packs=false
      this.bodymain=false
      this.profile=menu
    })
    this.EVENT.orderpacks.subscribe(menu=>{
      this.neworder=false;
      this.pendorder=false
      this.delivery_order=false
      this.order_delivered=false
      this.order_cancel=false
      this.order_packs=menu
      this.bodymain=false
      this.profile=false
    })
    this.EVENT.Neworders.subscribe(menu=>{
      this.neworder=menu;
      this.pendorder=false
      this.delivery_order=false
      this.order_delivered=false
      this.order_cancel=false
      this.order_packs=false
      this.bodymain=false
      this.profile=false
    })
    this.EVENT.ordercancel.subscribe(menu=>{
      this.neworder=false;
      this.pendorder=false
      this.delivery_order=false
      this.order_delivered=false
      this.order_cancel=menu
      this.order_packs=false
      this.bodymain=false
      this.profile=false
    })
    this.EVENT.orderdelivered.subscribe(menu=>{
      this.neworder=false;
      this.pendorder=false
      this.delivery_order=false
      this.order_delivered=menu
      this.order_cancel=false
      this.order_packs=false
      this.bodymain=false
      this.profile=false
    })
    this.EVENT.deliveryorders.subscribe(menu=>{
      this.neworder=false;
      this.pendorder=false
      this.delivery_order=menu
      this.order_delivered=false
      this.order_cancel=false
      this.order_packs=false
      this.bodymain=false
      this.profile=false
    })
    this.EVENT.pendorders.subscribe(menu=>{
      this.neworder=false;
      this.pendorder=menu
      this.delivery_order=false
      this.order_delivered=false
      this.order_cancel=false
      this.order_packs=false
      this.bodymain=false
      this.profile=false
    })

    this.EVENT.logout.subscribe(res=>{
      console.log(res)
      try{
        this.tokenService.signOut();
        setTimeout(()=>{
          location.href="/login"
        },1000)

      }catch(err){
        console.log(err)
      }
    })

    this.tokenService.getUser().then(
      (user)=>{
        console.log(user)
       if(user)
       this.menu.user=user.name;
       this.menu.role=user.role.role_name
      }
    ).catch((err)=>{
      console.log(err)
    });

    this.api.getOrdersByStatus("pending at production").subscribe(
      res=>{
        console.log(res)
        var pend:any=res
        if(pend.status){
          this.pending=pend.data
        }
      }
    )
    this.api.getOrdersByStatus("new").subscribe(
      res=>{
        console.log(res)
        var neworders:any=res
        if(neworders.status){
          this.new=neworders.data
        }
      }
    )
    this.api.getOrdersByStatus("production ended").subscribe(
      res=>{
        console.log(res)
        let order_packages=res
        if(order_packages.status){
          this.packetages=order_packages.data
        }
      }
    )
    this.api.getOrdersByStatus("pending delivery").subscribe(
      res=>{
        console.log(res)
        var order_pack=res
        if(order_pack.status){
          this.delivery=order_pack.data
        }
      }
    )
    this.api.getOrdersByStatus("Delivered").subscribe(
      res=>{
        console.log(res)
        var order_delivred=res
        if(order_delivred.status){
          this.delivered= order_delivred.data
        }
      }
    )
  }

}
