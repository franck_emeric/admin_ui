import { Component, OnInit, Input,OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-delivery-orders',
  templateUrl: './delivery-orders.component.html',
  styleUrls: ['./delivery-orders.component.scss']
})
export class DeliveryOrdersComponent implements OnInit,OnChanges {
@Input() delivery:any=[]
element:any=[]
search:any
  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(): void {
    this.element=this.delivery  
  }
  inputSearch(){
    if(this.search){
      this.element=this.delivery.filter((product:any)=>{
        if(product.userorder_id.toString().includes(this.search.toString()) ||
        product.user_first_name.toLowerCase().includes(this.search.toLowerCase())){
          return product
        } 
      })
    }else{
      this.element=this.delivery
    }
  }
}
