import { Component, OnInit,Input,OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-customer-new-orders',
  templateUrl: './customer-new-orders.component.html',
  styleUrls: ['./customer-new-orders.component.scss']
})
export class CustomerNewOrdersComponent implements OnInit,OnChanges {
@Input() News:any=[]
element:any=[]
search:any
  constructor() { }

  ngOnInit(): void {
  }
ngOnChanges(): void {
  this.element=this.News  
}

inputSearch(){
  if(this.search){
    this.element=this.News.filter((product:any)=>{
      if(product.userorder_id.toString().includes(this.search.toString()) ||
      product.user_first_name.toLowerCase().includes(this.search.toLowerCase())){
        return product
      }
    })
  }else{
    this.element=this.News
  }
}
}
