import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerNewOrdersComponent } from './customer-new-orders.component';

describe('CustomerNewOrdersComponent', () => {
  let component: CustomerNewOrdersComponent;
  let fixture: ComponentFixture<CustomerNewOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerNewOrdersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerNewOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
