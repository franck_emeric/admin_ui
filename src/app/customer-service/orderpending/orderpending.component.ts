import { Component, OnInit,Input,OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-orderpending',
  templateUrl: './orderpending.component.html',
  styleUrls: ['./orderpending.component.scss']
})
export class OrderpendingComponent implements OnInit,OnChanges {
@Input() Pending:any=[]
element:any=[]
search:any
  constructor() { }

  ngOnInit(): void {
  }
ngOnChanges(): void {
   this.element=this.Pending 
}
inputSearch(){
  if(this.search){
    this.element= this.Pending.filter((product:any)=>{
      if(product.userorder_id.toString().includes(this.search.toString()) ||
      product.user_first_name.toLowerCase().includes(this.search.toLowerCase()) ||
      product.user_last_name.toLowerCase().includes(this.search.toLowerCase())
      ){
        return product
      }
    })
  }else{
    this.element=this.Pending
  }
}
}
