import { Component, OnInit,Input,OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-orderpackatages',
  templateUrl: './orderpackatages.component.html',
  styleUrls: ['./orderpackatages.component.scss']
})
export class OrderpackatagesComponent implements OnInit,OnChanges {
@Input() packatages:any=[]
element:any=[]
search:any
  constructor() { }

  ngOnInit(): void {
  }
ngOnChanges(): void {
  this.element=this.packatages  
}

inputSearch(){
  if(this.search){
    this.element=this.packatages.filter((product:any)=>{
      if(product.userorder_id.toString().includes(this.search.toString()) ||
      product.user_first_name.toLowerCase().includes(this.search.toLowerCase()) ||
      product.user_last_name.toLowerCase().includes(this.search.toLowerCase())
      ){
        return product
      }
    })
  }else{
    this.element=this.packatages
  }
}
}
