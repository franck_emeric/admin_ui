import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderpackatagesComponent } from './orderpackatages.component';

describe('OrderpackatagesComponent', () => {
  let component: OrderpackatagesComponent;
  let fixture: ComponentFixture<OrderpackatagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderpackatagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderpackatagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
