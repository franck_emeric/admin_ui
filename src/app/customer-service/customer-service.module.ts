import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// MDB Angular Free

import { SharedModule } from '../shared';
import { CustomerServiceRoutingModule } from './customer-service-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomerNewOrdersComponent } from './customer-new-orders/customer-new-orders.component';
import { OrderpendingComponent } from './orderpending/orderpending.component';
import { OrderpackatagesComponent } from './orderpackatages/orderpackatages.component';
import { OrderDeliveredComponent } from './order-delivered/order-delivered.component';
import { OrderCancelComponent } from './order-cancel/order-cancel.component';
import { DeliveryOrdersComponent } from './delivery-orders/delivery-orders.component';
import { BodymainComponent } from './bodymain/bodymain.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  declarations: [
    DashboardComponent,
    CustomerNewOrdersComponent,
    OrderpendingComponent,
    OrderpackatagesComponent,
    OrderDeliveredComponent,
    OrderCancelComponent,
    DeliveryOrdersComponent,
    BodymainComponent,
    ProfileComponent
  ],
  imports: [
    CustomerServiceRoutingModule,
    SharedModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CustomerServiceModule { }
