import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    redirectTo: '/login',
    pathMatch: 'full',
},
  {path:'production/manager', loadChildren:()=>import('./production/production.module').then(prod=>prod.ProductionModule)},
  {path:'customer/services', loadChildren:()=>import('./customer-service/customer-service.module').then(cust=>cust.CustomerServiceModule)},
  {path:"me",loadChildren:()=>import('./admin/admin.module').then(admin=>admin.AdminModule)},
  {path:"designer",loadChildren:()=>import('./designer/designer.module').then(designer=>designer.DesignerModule)},
  {path:"delivery",loadChildren:()=>import('./delivery/delivery.module').then(delivery=>delivery.DeliveryModule)},
  {path:"partner",loadChildren:()=>import('./partner/partner.module').then(part=>part.PartnerModule)},
  {path:"stock/manager",loadChildren:()=>import('./stockmanagement/stockmanagement.module').then(stock=>stock.StockmanagementModule)},
  {path:"marketing/manager",loadChildren:()=>import('./marketing/marketing.module').then(market=>market.MarketingModule)},

  {path:'', loadChildren:()=>import('./auths/auths.module').then(auth=>auth.AuthsModule)},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
