import { TestBed } from '@angular/core/testing';

import { EventparserService } from './eventparser.service';

describe('EventparserService', () => {
  let service: EventparserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventparserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
