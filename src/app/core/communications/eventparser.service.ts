import { Injectable } from '@angular/core';
import {  Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EventparserService {
  
  design = new Subject<boolean>()
  profile = new Subject<boolean>()
  update = new Subject<boolean>()
  productlist = new Subject<boolean>()
  display = new Subject<boolean>()
  printed = new Subject<boolean>()
  gadget= new Subject<boolean>()
  packages = new Subject<boolean>()
  clothes = new Subject<boolean>()
  designdefault = new Subject<boolean>()
  loaded = new Subject<boolean>()
  logout = new Subject<boolean>()
  editor = new Subject<any>();
  iscategory =new Subject<any>()
  istype_prod =new Subject<any>()
  isElement = new Subject<any>()

  //production 
  producted_item=new Subject<boolean>()
  new_orders=new Subject<boolean>()
  pending= new Subject<boolean>()
  //update production
  update_ended_prod= new Subject<any>()
  update_pending_order= new Subject<any>()
  update_pending_delivery= new Subject<any>()
  uddate_delivered_orders= new Subject<any>()
  //end production
  add_user =new Subject<boolean>()
  list_user= new Subject<boolean>()
  Partner=new Subject<boolean>()
  Order_partner=new Subject<boolean>()
  Aprov_partner =new Subject<boolean>()
  Product_partner=new Subject<boolean>()

  //Customers service
  Neworders= new Subject<boolean>()
  pendorders= new Subject<boolean>()
  orderdelivered= new Subject<boolean>()
  deliveryorders= new Subject<boolean>()
  orderpacks= new Subject<boolean>()
  ordercancel= new Subject<boolean>()
  Bodymain= new Subject<boolean>()
  profile_customer = new Subject<boolean>()
  update_pro= new Subject<boolean>()
  add_pro = new Subject<boolean>()
  //delivery
  custom_design= new Subject<boolean>()
  pack_order= new Subject<boolean>()
  end_pack_order =new Subject<boolean>()
  delevered =new Subject<boolean>()
  profiles = new Subject<boolean>()
  userData=new Subject<any>();
  isloggedInAsSuperAdmin = new Subject<boolean>()
  productsaved = new Subject<boolean>()
  themesaved = new Subject<boolean>()
  addShape=new Subject<boolean>()
  constructor() { }
}
