import { Injectable } from '@angular/core';
import { EventparserService } from '../communications';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  BaseUrl= environment.baseUrlDev
  constructor(private myevent:EventparserService,private http:HttpClient) { }
 
  getOrders():Observable<any>{
    return this.http.get<any>(this.BaseUrl+"users/orders")
  
  } 

  getOrdersByStatus(status:string):Observable<any>{
    return this.http.get<any>(this.BaseUrl+"users/orders/"+status)
  }

  inviteUser(data:any){
    return this.http.post<any>(this.BaseUrl+"admin/invite",data)

  }

  setStatus(id:string|any,status:string):Observable<any>{
    return this.http.patch<any>(this.BaseUrl+"users/orders/"+id,{status:status})
  }

  saveDesigns(data:any):Observable<any>{
    return  this.http.post<any>(this.BaseUrl+"products/designs",data)

  }

  upDateProduct(data:any):Observable<any>{
    return this.http.patch(this.BaseUrl+"products",data)

  }

  upDateUser(data:any){
    return this.http.patch<any>(this.BaseUrl+"admin",data)
  }


  saveUser(data:any):Observable<any>{
    return this.http.post<any>(this.BaseUrl+"admin",data)
  }

  setUserRole(data:any):Observable<any>{
    return this.http.patch(this.BaseUrl+"admin/roles",data)
  }
  postUserRole(data:any):Observable<any>{
    return this.http.post(this.BaseUrl+"admin/roles",data)
  }


  getProducts(name:any):Observable<any>{
    return this.http.get<any>(this.BaseUrl+"products/"+name)
  }


  saveProduct(data:any):Observable<any>{
    return  this.http.post<any>(this.BaseUrl+"products",data)
  }

  getProductType():Observable<any>{
    return this.http.get<any>(this.BaseUrl+"products/type")
  }

  getType=(category:string):Observable<any>=>{
    return this.http.get<any>(this.BaseUrl+"products/type/"+category)
  }

  getAllCategories():Observable<any>{
    return this.http.get<any>(this.BaseUrl+"products/category")
  }

  saveTheme(data:any):Observable<any>{
    return  this.http.post<any>(this.BaseUrl+"editor/themes",data)
  }

  saveItem(data:any):Observable<any>{
    return  this.http.post<any>(this.BaseUrl+"editor/items",data)
  }


  geTItem(id:any):Observable<any>{
    return  this.http.get<any>(this.BaseUrl+"editor/items/"+id)
  }
  
  getType_theme(id:any):Observable<any>{
    return this.http.get<any>(this.BaseUrl+"editor/themes/"+id)
  }

  saveFile(formdata:any):Observable<any>{

    return this.http.post<any>(this.BaseUrl+"upload",formdata)
  }
  //categories

  Add_category(data:any):Observable<any>{
    return this.http.post<any>(this.BaseUrl+"products/category", data)
  }
//productType

Add_Type(data:any):Observable<any>{
  return this.http.post<any>(this.BaseUrl+"products/type", data)
}
//printing
Add_printing_type(data:any):Observable<any>{
  return this.http.post<any>(this.BaseUrl+"products/printer", data)
}
//format

Add_format(data:any):Observable<any>{
  return this.http.post<any>(this.BaseUrl+"products/format", data)
}
Add_mode(data:any):Observable<any>{
  return this.http.post<any>(this.BaseUrl+"products/mode", data)
}
}
