import { Injectable } from '@angular/core';
import { EventparserService } from '../communications';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { TokenStorageServiceService } from './token-storage-service.service';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
baseUrl=environment.baseUrlDev
routes=["designer","customer_service","production","admin"]

constructor(private event:EventparserService,private http:HttpClient,private tokenStorage: TokenStorageServiceService) { }

auth=(data:any)=>
{
  this.http.post(this.baseUrl+"admin/login",data).subscribe(
    res=>{
     var resp:any=res 
     if(resp.status){
      this.tokenStorage.saveToken(resp.data.token.access_token);
      this.tokenStorage.saveRefreshToken(resp.data.token.refresh_token);
      var ob:object={
        name:resp.data.admin_first_name,
        id:resp.data.admin_id,
        email:resp.data.admin_email,
        role:resp.data.role,
        perm:resp.data.perm
      }
      this.event.loaded.next(true)
      var role =JSON.parse(resp.data.role)
      if(role===this.routes[3]){
        try{
         this.tokenStorage.saveUser(ob)

          location.href="/me"
         }catch(err){
               console.log(err)
             }

      }

      if(role===this.routes[0]){
        try{
         this.tokenStorage.saveUser(ob)
          location.href="/designer"
         }catch(err){
        console.log(err)
        }

      }

      if(role===this.routes[1]){
        try{
         this.tokenStorage.saveUser(ob)
          location.href="/customer/services"
         }catch(err){
               console.log(err)
             }

      }


      if(role===this.routes[2]){
        try{
         this.tokenStorage.saveUser(ob)
          location.href="/production/manager"
         }catch(err){
               console.log(err)
             }

      }

      

     }
    }
    ,
   err=>{
    this.event.loaded.next(false) 
    console.log(err)
    if(err.error.statusCode==401){
      Swal.fire(
        {
          icon:"error",
          text:"vous n'etes pas apte à utiliser ce service  "
        })
    }

    if(err.error.statusCode==400){
      Swal.fire(
        {
          icon:"error",
          text:"mauvaise requete"
        })
    }
   }
   )
}

}
