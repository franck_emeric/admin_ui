import { Injectable } from '@angular/core';
const TOKEN_KEY = 'auth-token';
const REFRESHTOKEN_KEY = 'auth-refreshtoken';
const USER_KEY="user"
import { compress, decompress } from 'compress-json'
@Injectable({
  providedIn: 'root'
})
export class TokenStorageServiceService {

  constructor() { }


  signOut(): void {
    try{
     // window.localStorage.clear();
      localStorage.removeItem(TOKEN_KEY);
      localStorage.removeItem(REFRESHTOKEN_KEY);
      localStorage.removeItem(USER_KEY)

    }catch(err){
      console.log(err)
    }
  }

  public saveToken(token: string): void {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return window.localStorage.getItem(TOKEN_KEY);
  }


  public saveRefreshToken(token: string): void {
    window.localStorage.removeItem(REFRESHTOKEN_KEY);
    window.localStorage.setItem(REFRESHTOKEN_KEY, token);
  }

  public getRefreshToken(): string | null {
    return window.localStorage.getItem(REFRESHTOKEN_KEY);
  }

  public saveUser(data:object){
    let compressed:any = compress(data)
    window.localStorage.setItem(USER_KEY,JSON.stringify(compressed))
  }

  public async getUser(){
    var compressed:string|any= localStorage.getItem(USER_KEY);
    var decompressed= decompress(JSON.parse(compressed))
    return decompressed
  }
}
