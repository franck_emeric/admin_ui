import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError,BehaviorSubject } from 'rxjs';
import {TokenStorageServiceService} from './';
import { tap ,catchError, filter, switchMap, take} from 'rxjs/operators';
import { LoginService } from './login.service';
@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private tokenservice:TokenStorageServiceService, private authService:LoginService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.getAuthToken();
    if (token) {
      // If we have a token, we set it to the header
      req = req.clone({
         setHeaders: {"x-access-token": `${token}`}
      });
      
   }
   return next.handle(req).pipe(
     tap(event=>{
     },
     err=>{
       if(!req.url.includes('admin/login') && err.status === 401){
         console.log(req)
         location.href="/"
         this.handle401Error(req,next)
       }
      
     }
     )
   )
  }
  getAuthToken():string {
    const token:any= this.tokenservice.getToken()
    return token
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
      if (!this.isRefreshing) {
        this.isRefreshing = true;
        this.refreshTokenSubject.next(null);
        const token = this.getAuthToken();
  
        if (token)
          return this.authService.refresh(token).pipe(
            switchMap((token: any) => {
              this.isRefreshing = false;
  
              this.tokenservice.saveToken(token.access_token);
              this.refreshTokenSubject.next(token.access_token);
              
              return next.handle(this.addTokenHeader(request, token.access_token));
            }),
            catchError((err) => {
              this.isRefreshing = false;    
              this.tokenservice.signOut();
              return throwError(err);
            })
          );
      }
  
      return this.refreshTokenSubject.pipe(
        filter(token => token !== null),
        take(1),
        switchMap((token) => next.handle(this.addTokenHeader(request, token)))
      );
    }

    private addTokenHeader(request: HttpRequest<any>, token: string) {
  
      return request.clone({ headers: request.headers.set("x-access-token", token) });
    }
  
}

