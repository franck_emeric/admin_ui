import { TestBed } from '@angular/core/testing';

import { AthAdminGuard } from './ath-admin.guard';

describe('AthAdminGuard', () => {
  let guard: AthAdminGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AthAdminGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
