import { TestBed } from '@angular/core/testing';

import { CustomerServiceGuard } from './customer-service.guard';

describe('CustomerServiceGuard', () => {
  let guard: CustomerServiceGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CustomerServiceGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
