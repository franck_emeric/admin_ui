import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageServiceService } from '../..';
import {JwtHelperService} from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class PartnerGuard implements CanActivate {
  constructor(private tokenService:TokenStorageServiceService,private jwt_helper:JwtHelperService){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      var token:string|any= this.tokenService.getToken();
      var isExpired= this.jwt_helper.isTokenExpired(token);
      if(isExpired){
       location.href= "/"
        return false
      }else{
        return true;
      }
  }
  
}
