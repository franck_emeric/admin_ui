import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class CanRegisterGuard implements CanActivate {
  constructor(private jwt_helper:JwtHelperService){}
  canActivate(
    route: ActivatedRouteSnapshot,
  
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
     
      console.log(route.params.token,125)
      var token=route.params.token
      const isExpired = this.jwt_helper.isTokenExpired(token);
      console.log(token)
      if(isExpired){
        location.href="/link-expired"
        return false
      }else{
        return true
      }
  }
  
}
